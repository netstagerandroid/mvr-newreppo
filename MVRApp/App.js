// In App.js in a new project

import * as React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer';
import { app_background, app_red, border_gray } from './src/constants/colors';
import { SplashScreen } from './src/screens/SplashScreen';
import { HomeScreen } from './src/screens/HomeScreen';
import { CommissionScreen } from './src/screens/CommissionScreen';
import { ApplicationsScreen } from './src/screens/ApplicationsScreen';
import { GenerateLinkScreen } from './src/screens/GenerateLinkScreen';
import { ProfileScreen } from './src/screens/ProfileScreen';
import { ChangePassword } from './src/screens/ChangePassword';
import { DrawerContent } from './src/components/DrawerContent';
import { RedeemRequest } from './src/screens/RedeemRequest';
import { LoginScreen } from './src/screens/LoginScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { GET_PROFILE } from './src/constants/networkConstants';
import { ForgotPassword } from './src/screens/ForgotPassword';
import { NewApplicationScreen } from './src/screens/NewApplicationScreen';
import { PaymentGatewayScreen } from './src/screens/PaymentGatewayScreen';
import { BenificiaryScreen } from './src/screens/BenificiaryScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function DrawerNavigator(props) {
  const { navigate } = useNavigation();
  const [response, setResponse] = React.useState();
  const [currentScreen, setCurrentScreen] = React.useState();
  async () => {
    await AsyncStorage.getItem('Screen').then((scrn) => setCurrentScreen(scrn));
  };
  async () => {
    await AsyncStorage.getItem('response').then((resp) => setResponse(resp));
  };
  return (
    <Drawer.Navigator
      headerMode="none"
      initialRouteName="ApplicationsScreen"
      backBehavior="initialRoute"
      drawerContent={(props) => (
        <DrawerContent
          name={response && response.Name}
          email={response && response.Email}
          currentScrn={currentScreen}
          {...props}
        />
      )}
    >
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="GenerateLinkScreen" component={GenerateLinkScreen} />
      <Stack.Screen name="ApplicationsScreen" component={ApplicationsScreen} />
      <Stack.Screen name="CommissionScreen" component={CommissionScreen} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
      <Stack.Screen name="NewApplicationScreen" component={NewApplicationScreen} />
    </Drawer.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        {/* <Stack.Screen name="PaymentGatewayScreen" component={PaymentGatewayScreen} /> */}

        {/* <Stack.Screen name="BenificiaryScreen" component={BenificiaryScreen} /> */}
        {/* <Stack.Screen name="NewApplicationScreen" component={NewApplicationScreen} /> */}
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />

        <Stack.Screen name="DrawerNavigator" component={DrawerNavigator} />

        {/* <Stack.Screen name="HomeScreen" component={DrawerNavigator} /> */}
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="PaymentGatewayScreen" component={PaymentGatewayScreen} />
        {/* <Stack.Screen name="CommissionScreen" component={CommissionScreen} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
