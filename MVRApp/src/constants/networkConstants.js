// export const BASE_URL = 'http://mvrapi.netstager.com';
export const BASE_URL = 'https://mvrapi.calicutcitybank.com';
export const GET_APPLICATIONS = BASE_URL + '/api/Applications/GetApplications/';
export const GET_APPLICATIONS_FILTER = BASE_URL + '/api/Applications/GetFilterApplications/';
export const GET_COMMISSION = BASE_URL + '/api/Commision/GetCommsions/';
export const GET_COMMISSION_FILTER = BASE_URL + '/api/Commision/GetFilterCommsions/';
export const LOGIN = BASE_URL + '/api/Login/Login';
export const GET_PROFILE = BASE_URL + '/api/Login/Profile/';
export const CHANGE_USER_INFO = BASE_URL + '/api/Login/UserChangeInfo';
export const CHANGE_PASSWORD = BASE_URL + '/api/Login/UserChangePassword';
export const FORGOT_PASSWORD = BASE_URL + '/api/Login/ForgotPassword';
export const SAVE_APPLICATION = BASE_URL + '/api/Applications/SaveApplication';
export const PAGE_LOAD = BASE_URL + '/api/Applications/PageLoad/';

