import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

import * as icons from '../constants/icons';
import { app_background, app_red } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';

// export default class AppBar extends React.Component {

export const Button = (props) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={props.onPress}
      style={[styles.container, props.width]}
    >
      <View style={styles.redeemBorder}>
        <Text style={styles.text}>{props.text}</Text>
      </View>
    </TouchableOpacity>
  );
};
// }

const styles = StyleSheet.create({
  container: {
    backgroundColor: app_red,
    height: 45,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
  },
  redeemBorder: {
    borderWidth: 0.5,
    borderRadius: 9,
    width: '100%',
    height: '100%',
    borderColor: '#FBF7F7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    padding: 10,
  },
});
