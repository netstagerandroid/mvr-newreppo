import * as React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
  Alert,
} from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerItemList,
  DrawerItem,
  DrawerContentScrollView,
  DrawerView,
} from '@react-navigation/drawer';
import {
  ic_application_selected,
  ic_application_unselected,
  ic_commission_selected,
  ic_commission_unselected,
  ic_link_selected,
  ic_link_unselected,
  ic_key_selected,
  ic_key_unselected,
  ic_logout_unselected,
  ic_logout_selected,
} from '../constants/icons';
import { useState } from 'react';
import { app_background, app_red } from '../constants/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { GET_PROFILE } from '../constants/networkConstants';
import { IS_LOGGED_IN, UserId } from '../constants/appConstants';

const data = [
  {
    selectedImg: ic_application_selected,
    image: ic_application_unselected,
    name: 'New Application',
  },
  {
    selectedImg: ic_application_selected,
    image: ic_application_unselected,
    name: 'New Application',
  },
  {
    selectedImg: ic_application_selected,
    image: ic_application_unselected,
    name: 'New Application',
  },
  {
    selectedImg: ic_application_selected,
    image: ic_application_unselected,
    name: 'New Application',
  },
];

export function DrawerContent(props) {
  const { navigate } = useNavigation();
  const [selectedNav, setSelectedNav] = React.useState();
  const [name, setName] = React.useState();
  const [email, setEmail] = React.useState();

  props.currentScrn && setSelectedNav(props.currentScrn);
  console.log(props.currentScrn);
  const setNav = async (value) =>
    await AsyncStorage.setItem('screen', value).then((scrn) => {
      setSelectedNav(scrn);
    });
  const showLogout = () => {
    Alert.alert('Logout', 'Do you want to logout?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: async () => {
          try {
            await AsyncStorage.setItem(UserId, null)
              .then(AsyncStorage.setItem(IS_LOGGED_IN, 'false'))
              .then(props.navigation.replace('LoginScreen'));
          } catch (e) {
            console.log(e);
          }
        },
      },
    ]);
  };

  const initScrn = async () => {
    await AsyncStorage.getItem('name').then((value) => {
      setName(value);
    });
    await AsyncStorage.getItem('email').then((value) => {
      setEmail(value);
    });
    await AsyncStorage.getItem('screen').then((value) => {
      setSelectedNav(value);
    });
  };
  initScrn();

  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView>
        <View style={{ marginLeft: 20, marginTop: 20 }}>
          {/* <Image
            style={{ width: 100, height: 100, borderRadius: 50, overflow: 'hidden' }}
            source={require('../assets/images/login_image.png')}
          /> */}
          <Text style={styles.nameText}>{name}</Text>
          <Text style={styles.emailText}>{email}</Text>
        </View>

        <TouchableOpacity
          activeOpacity={0.5}
          style={[
            styles.tilesItem,
            { marginTop: 10 },
            selectedNav == 'ApplicationsScreen' && { backgroundColor: app_background },
          ]}
          onPress={() => {
            setNav('ApplicationsScreen');
            props.navigation.navigate('NewApplicationScreen');
            setSelectedNav('ApplicationsScreen');
          }}
        >
          <Image
            resizeMode={'contain'}
            style={[styles.tileIcon, { width: 15, height: 15 }]}
            source={
              selectedNav == 'ApplicationsScreen'
                ? ic_application_selected
                : ic_application_unselected
            }
          />
          <Text
            style={[styles.tileItemText, selectedNav == 'ApplicationsScreen' && { color: app_red }]}
          >
            New Application
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.5}
          style={[
            styles.tilesItem,
            selectedNav == 'CommissionScreen' && { backgroundColor: app_background },
          ]}
          onPress={() => {
            setNav('CommissionScreen');
            props.navigation.navigate('CommissionScreen');
            setSelectedNav('CommissionScreen');
          }}
        >
          <Image
            resizeMode={'contain'}
            style={styles.tileIcon}
            source={
              selectedNav == 'CommissionScreen' ? ic_commission_selected : ic_commission_unselected
            }
          />
          <Text
            style={[styles.tileItemText, selectedNav === 'CommissionScreen' && { color: app_red }]}
          >
            Commission
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.5}
          style={[
            styles.tilesItem,
            selectedNav == 'GenerateLinkScreen' && { backgroundColor: app_background },
          ]}
          onPress={() => {
            setNav('GenerateLinkScreen');
            props.navigation.navigate('GenerateLinkScreen');
            setSelectedNav('GenerateLinkScreen');
          }}
        >
          <Image
            resizeMode={'contain'}
            style={styles.tileIcon}
            source={selectedNav == 'GenerateLinkScreen' ? ic_link_selected : ic_link_unselected}
          />
          <Text
            style={[styles.tileItemText, selectedNav == 'GenerateLinkScreen' && { color: app_red }]}
          >
            Generate Link & Share
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.5}
          style={[
            styles.tilesItem,
            selectedNav == 'ChangePassword' && { backgroundColor: app_background },
          ]}
          onPress={() => {
            setNav('ChangePassword');
            props.navigation.navigate('ChangePassword');
            setSelectedNav('ChangePassword');
          }}
        >
          <Image
            resizeMode={'contain'}
            style={styles.tileIcon}
            source={selectedNav === 'ChangePassword' ? ic_key_selected : ic_key_unselected}
          />
          <Text
            style={[styles.tileItemText, selectedNav === 'ChangePassword' && { color: app_red }]}
          >
            Change Password
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.5}
          style={[styles.tilesItem, selectedNav == 'Logout' && { backgroundColor: app_background }]}
          onPress={() => {
            showLogout();
            setNav('Logout');
            // props.navigation.navigate('ChangePassword');
            setSelectedNav('Logout');
          }}
        >
          <Image
            resizeMode={'contain'}
            style={styles.tileIcon}
            source={selectedNav === 'Logout' ? ic_logout_selected : ic_logout_unselected}
          />
          <Text style={[styles.tileItemText, selectedNav === 'Logout' && { color: app_red }]}>
            Logout
          </Text>
        </TouchableOpacity>
      </DrawerContentScrollView>
    </View>
  );
}
const styles = StyleSheet.create({
  nameText: {
    fontSize: 16,
    marginTop: 5,
    paddingHorizontal: 5,
    fontFamily: 'Nunito',
    textAlign: 'left',
    fontWeight: 'bold',
  },
  emailText: {
    fontSize: 12,
    fontFamily: 'Nunito',
    marginTop: 1,
    paddingHorizontal: 5,
    textAlign: 'left',
  },
  tilesItem: {
    flex: 1,
    marginHorizontal: 10,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderRadius: 8,
    flexDirection: 'row',
  },
  tilesSelected: {
    backgroundColor: '#ECD7D7',
  },
  tileIcon: {
    width: 15,
    margin: 5,
    height: 20,
    padding: 2,
  },
  tileItemText: {
    fontSize: 14,
    color: '#474545',
    marginLeft: 5,
    fontFamily: 'Nunito',
    fontWeight: '100',
  },
});
