import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

import { app_background, app_red } from '../constants/colors';

export const Loader = (props) => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color={app_red} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: app_background,
    opacity: 0.8,
    elevation: 50,
    position: 'absolute',
  },
});
