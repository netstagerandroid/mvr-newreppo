import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

import * as icons from '../constants/icons';
import { app_background, app_red } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';

// export default class AppBar extends React.Component {

export const AppBar = (props) => {
  const navigation = useNavigation();
  return (
    <>
      {props.hideButtons ? (
        <View style={[styles.container, { justifyContent: 'center' }]}>
          <Image style={styles.logo} source={icons.mvr_logo} />
        </View>
      ) : (
        <View style={styles.container}>
          <TouchableOpacity style={styles.touchable} onPress={navigation.toggleDrawer}>
            <Image source={icons.ic_menu} />
          </TouchableOpacity>
          <Image style={styles.logo} source={icons.mvr_logo} />
          <TouchableOpacity
            onPress={() => navigation.navigate('ProfileScreen')}
            style={styles.touchable}
          >
            <Image style={styles.image} source={icons.ic_user} />
          </TouchableOpacity>
        </View>
      )}
    </>
  );
};
// }

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    paddingHorizontal: 10,
    marginTop: 10,
    paddingBottom: 5,
  },
  touchable: {
    alignSelf: 'center',
  },
  image: {
    width: 28,
    height: 28,
  },
  logo: {
    width: 100,
    height: 40,
    alignSelf: 'center',
  },
});
