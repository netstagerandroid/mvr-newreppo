// import Toast from 'react-native-simple-toast';
// import NetworkConstant from '../constants/NetworkConstant';
// var qs = require('qs');

export const setConnected = (status) => {
  return {
    type: 'LOADING',
    payload: status,
  };
};

export const setLogged = (status) => {
  return {
    type: 'LOGGED',
    payload: status,
  };
};
