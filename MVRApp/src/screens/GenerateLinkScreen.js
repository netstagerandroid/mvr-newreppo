import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Share,
  ToastAndroid,
} from 'react-native';

import { ic_share } from '../constants/icons';
import { app_red, border_gray, item_background_gray } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';
import { AppBar } from '../components/AppBar';
import { Button } from '../components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UserId } from '../constants/appConstants';

export const GenerateLinkScreen = (props) => {
  const { navigate } = useNavigation();
  const [generatedUrl, setGenerateUrl] = useState('');
  const url = 'https://masscare.calicutcitybank.com/Default.aspx?agentId=';
  const msg = 'Please check the link';
  const generateUrl = async () => {
    await AsyncStorage.getItem(UserId).then((id) => {
      setGenerateUrl(url + id);
    });
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: msg + ' ' + generatedUrl,
        url: generatedUrl,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      // alert(error.message);
    }
  };
  generateUrl();

  const initScrn = async () => await AsyncStorage.setItem('screen', 'GenerateLinkScreen');
  initScrn();
  return (
    <View style={styles.container}>
      <AppBar />
      <ScrollView showsHorizontalScrollIndicator={false}>
        <Text style={styles.titleText}>Generate & Share</Text>
        <View style={styles.searchBar}>
          <TextInput
            style={styles.textInput}
            returnKeyType="search"
            onSubmitEditing={() => alert(searchText)}
            placeholder={'Generated Link'}
            onChangeText={(text) => onChangeSearchText(text)}
            value={generatedUrl}
          />
        </View>
        <View style={styles.buttonsView}>
          <Button width={{ width: '40%' }} text={'Generate'} onPress={generateUrl} />
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => {
              generatedUrl ? onShare() : generateUrl().then(onShare);
            }}
            style={[styles.shareButton]}
          >
            <View style={styles.redeemBorder}>
              <Image style={{ width: 20, height: 20 }} source={ic_share} />
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    // backgroundColor: app_background,
    flex: 1,
  },
  searchBar: {
    flexDirection: 'row',
    width: '80%',
    height: 50,
    borderColor: border_gray,
    borderWidth: 1,
    borderRadius: 15,
    alignSelf: 'center',
    paddingHorizontal: 10,
    marginTop: 20,
  },
  titleText: {
    fontSize: 26,
    paddingHorizontal: 10,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
  },
  textInput: {
    paddingHorizontal: 15,
    flexGrow: 1,
  },
  buttonsView: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  shareButton: {
    backgroundColor: app_red,
    height: 45,
    width: 50,
    marginStart: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
  },
  redeemBorder: {
    borderWidth: 0.5,
    borderRadius: 9,
    width: '100%',
    height: '100%',
    borderColor: '#FBF7F7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    padding: 10,
  },
});
