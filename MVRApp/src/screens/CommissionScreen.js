import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image, TextInput, ScrollView, ToastAndroid } from 'react-native';

import { ic_search } from '../constants/icons';
import { app_red, border_gray, item_background_gray } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';
import { AppBar } from '../components/AppBar';
import { Button } from '../components/Button';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Loader } from '../components/Loader';
import { GET_COMMISSION, GET_COMMISSION_FILTER } from '../constants/networkConstants';
import { UserId } from '../constants/appConstants';

export const CommissionScreen = (props) => {
  const { navigate } = useNavigation();
  const [searchText, onChangeSearchText] = useState('');
  const dummyData = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  const [response, setResponse] = useState();
  const [userId, setUserId] = useState('');
  const [isLoading, setLoading] = useState(true);
  const initScrn = async () => await AsyncStorage.setItem('screen', 'CommissionScreen');
  initScrn();
  const apiGetCommission = async (id) => {
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    setLoading(true);
    // var body = {
    //   UserId: 'test',
    //   Password: '1234',
    // };

    // await fetch('http://mvrapi.netstager.com/api/Login/Login', {
    //   method: 'POST',
    //   headers: headers,
    //   body: JSON.stringify(body),
    // })
    //   .then((responsel) => responsel.json())
    //   .then((responseJson) => {
    //     console.log(responseJson);
    //     // setUserId(responseJson.UserId);
    //   })
    //   .catch((err) => console.log(err));

    await fetch(GET_COMMISSION + id, {
      headers: headers,
    })
      .then((respons) => Promise.all([respons.status, respons.json()]))
      .then((responseJson) => {
        setLoading(false);
        console.log(GET_COMMISSION + id);
        console.log(JSON.stringify(responseJson));
        if (responseJson[0] === 200)
          if (responseJson[1].IsSuccess) {
            setResponse(responseJson[1]);
          } else {
            alert(responseJson[1].Message);
          }
        else if (responseJson[0] === 401) {
          AsyncStorage.clear().then(navigate('LoginScreen'));
        } else {
          alert(responseJson[1].Message);
        }
      })
      .catch((err) => console.log(err))
      .finally(() => setLoading(false));
  };
  const apiSearchCommission = async (showLoading) => {
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    showLoading && setLoading(true);

    await fetch(GET_COMMISSION_FILTER + searchText, {
      headers: headers,
    })
      .then((respons) => Promise.all([respons.status, respons.json()]))
      .then((responseJson) => {
        setLoading(false);

        console.log(GET_COMMISSION_FILTER + searchText);
        console.log(JSON.stringify(responseJson));
        if (responseJson[0] === 200)
          if (responseJson[1].IsSuccess) {
            setResponse(responseJson[1]);
          } else {
            alert(responseJson[1].Message);
          }
        else if (responseJson[0] === 401) {
          AsyncStorage.clear().then(navigate('LoginScreen'));
        } else {
          alert(responseJson[1].Message);
        }
      })
      .catch((err) => console.log(err))
      .finally(() => setLoading(false));
  };
  useEffect(() => {
    AsyncStorage.getItem(UserId).then((id) => {
      apiGetCommission(id);
    });
  }, []);
  return (
    <View style={styles.container}>
      <AppBar />
      {isLoading && <Loader />}
      <ScrollView showsHorizontalScrollIndicator={false}>
        <Text style={styles.titleText}>Commission</Text>
        {/* Total View */}
        <View style={styles.totalView}>
          {/* Total Amount */}
          <View style={styles.totalItem}>
            <Text style={styles.amountTitle}>
              ₹ {response && response['entity'].TotalCommision}
            </Text>
            <Text style={styles.amountSubTitle}>Total Amount</Text>
          </View>
          {/* redeem Amount */}
          <View style={styles.totalItem}>
            <Text style={styles.amountTitle}>
              ₹ {response && response['entity'].RedeemedAmount}
            </Text>
            <Text style={styles.amountSubTitle}>Total Amount Redeemed</Text>
          </View>
          {/* Balance Amount */}
          <View style={styles.totalItem}>
            <Text style={styles.amountTitle}>
              ₹{' '}
              {Number(response && response['entity'].TotalCommision) -
                Number(response && response['entity'].RedeemedAmount)}
            </Text>
            <Text style={styles.amountSubTitle}>Balance</Text>
          </View>
        </View>
        {/* Redeem Request */}
        {/* <View style={styles.button}>
          <Button text={'Redeem Request'} onPress={() => apiCall()} width={{ width: '50%' }} />
        </View> */}

        {/* Search Bar */}
        <View style={styles.searchBar}>
          <Image style={styles.searchIcon} source={ic_search} />
          <TextInput
            style={styles.textInput}
            keyboardType={'number-pad'}
            returnKeyType="search"
            onSubmitEditing={() => {
              searchText.length > 0
                ? apiSearchCommission(true)
                : ToastAndroid.show('Type amount to search', ToastAndroid.SHORT);
            }}
            placeholder={'Search'}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace') {
                searchText.length === 1 &&
                  AsyncStorage.getItem(UserId).then((id) => {
                    apiGetCommission(id);
                  });
              }
            }}
            onChangeText={(text) => {
              onChangeSearchText(text);
              // searchText && apiSearchCommission(false);
            }}
          />
          {searchText.length > 0 && (
            <Text
              style={{ color: app_red, fontSize: 12, alignSelf: 'center' }}
              onPress={() => {
                searchText.length > 0 && apiSearchCommission();
              }}
              source={ic_search}
            >
              Search
            </Text>
          )}
        </View>
        <View style={styles.flatlistView}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={response && response.entity.AgentsApplications}
            renderItem={(item) => (
              <View style={styles.itemList}>
                <Text style={styles.itemText}>{item.item.Id}</Text>
                <Text style={styles.itemText2}>{item.item.Amount}</Text>
                <Text style={[styles.itemText2, { fontSize: 11 }]}>{item.item.Date}</Text>
              </View>
            )}
            ListHeaderComponent={() => (
              <View style={styles.flatlistTitle}>
                <Text style={styles.listTitleText}>Apl. No</Text>
                <Text style={styles.listTitleText}>Commission</Text>
                <Text style={styles.listTitleText}>Date</Text>
              </View>
            )}
          />
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    // backgroundColor: app_background,
    flex: 1,
  },
  searchBar: {
    flexDirection: 'row',
    width: '90%',
    height: 40,
    borderColor: border_gray,
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 20,
  },
  searchIcon: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    padding: 5,
    marginRight: 5,
  },
  titleText: {
    fontSize: 26,
    paddingHorizontal: 10,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
  },
  totalView: {
    backgroundColor: app_red,
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    borderRadius: 10,
    padding: 5,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    paddingVertical: 20,
  },
  totalItem: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  amountTitle: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  amountSubTitle: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    marginTop: 20,
  },
  redeemBorder: {
    borderWidth: 1,
    borderColor: '#FBF7F7',
  },
  textInput: {
    flex: 1,
  },
  flatlistView: {
    marginTop: 10,
    marginHorizontal: 10,
  },
  flatlistTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    paddingHorizontal: '4%',
  },
  listTitleText: {
    color: app_red,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    fontSize: 13,
    flexGrow: 1,
    textAlign: 'center',
  },
  itemList: {
    backgroundColor: item_background_gray,
    borderRadius: 15,
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingRight: 15,
    paddingStart: 5,
    marginHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 8,
  },
  itemText: {
    fontWeight: 'bold',
    fontSize: 13,
    flexGrow: 1,
    color: 'black',
    textAlign: 'center',
  },
  itemText2: {
    fontWeight: 'bold',
    fontSize: 13,
    flexGrow: 1,
    color: 'black',
    textAlign: 'right',
  },
});
