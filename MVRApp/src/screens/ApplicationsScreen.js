import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  ToastAndroid,
  FlatList,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
} from 'react-native';

import { ic_search, ic_plus } from '../constants/icons';
import { app_background, app_red, border_gray, item_background_gray } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';
import { AppBar } from '../components/AppBar';
import { Loader } from '../components/Loader';
import { GET_APPLICATIONS, GET_APPLICATIONS_FILTER } from '../constants/networkConstants';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { IS_LOGGED_IN, UserId } from '../constants/appConstants';
import Moment from 'moment';
import moment from 'moment';

export const ApplicationsScreen = (props) => {
  const { navigate } = useNavigation();
  const [searchText, onChangeSearchText] = useState('');
  const [response, setResponse] = useState();
  const [userId, setUserId] = useState('');
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    try {
      AsyncStorage.getItem(UserId)
        .then((v) => {
          apiGetApplication(v);
          // setUserId(v);
        })
        .then();
    } catch (error) {}
  }, []);

  const initScrn = async () => await AsyncStorage.setItem('screen', 'ApplicationsScreen');
  initScrn();

  const apiSearchApplication = async (showLoading) => {
    showLoading && setLoading(true);
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    await fetch(GET_APPLICATIONS_FILTER + searchText, {
      headers: headers,
    })
      .then((responsea) => {
        alert('hi');
        console.log('hello');
        console.log(responsea);
        return Promise.all([responsea.status, responsea.json()]);
      })
      .then((responseJson) => {
        console.log('hi');
        console.log(responseJson);
        setLoading(false);
        if (responseJson[0] === 200)
          if (responseJson[1].IsSuccess) {
            setResponse(responseJson[1]);
          } else {
            alert(responseJson[1].Message);
          }
        else if (responseJson[0] === 401) {
          AsyncStorage.clear().then(navigate('LoginScreen'));
        } else {
          alert('added successfully');
          alert(responseJson[1].Message);
        }
      })
      .catch((err) => console.log(err));
  };

  const apiGetApplication = async (id) => {
    setLoading(true);
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    await fetch(GET_APPLICATIONS + id, {
      headers: headers,
    })
      .then((responsea) => {
        console.log('kk');
        console.log(responsea);
        return Promise.all([responsea.status, responsea.json()]);
      })
      .then((responseJson) => {
        console.log('jj');
        console.log(JSON.stringify(responseJson));
        setLoading(false);
        if (responseJson[0] === 200)
          if (responseJson[1].IsSuccess) {
            setResponse(responseJson[1]);
          } else {
            alert(responseJson[1].Message);
          }
        else if (responseJson[0] === 401) {
          AsyncStorage.clear().then(navigate('LoginScreen'));
        } else {
          alert(responseJson[1].Message);
        }
      })

      .catch((err) => console.log(err));
  };

  const permissionRequest = async () => {
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission',
          message: 'App needs to access files',
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        navigate('NewApplicationScreen');
      } else {
        alert('Permission Denied');
      }
    } else {
      navigate('NewApplicationScreen');
    }
  };

  return (
    <View style={styles.container}>
      <AppBar />
      {isLoading && <Loader />}
      <ScrollView showsHorizontalScrollIndicator={false}>
        <Text style={styles.titleText}>Applications</Text>

        {/* Search Bar */}
        <View style={styles.searchBar}>
          <Image style={styles.searchIcon} source={ic_search} />
          <TextInput
            style={styles.textInput}
            returnKeyType="search"
            onSubmitEditing={() => {
              searchText
                ? apiSearchApplication(true)
                : ToastAndroid.show('Type Something to search', ToastAndroid.SHORT);
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace') {
                searchText.length === 1 &&
                  AsyncStorage.getItem(UserId).then((id) => {
                    apiSearchApplication(id);
                  });
              }
            }}
            placeholder={'Search'}
            onChangeText={(text) => {
              onChangeSearchText(text);
              // searchText && apiSearchApplication(false);
            }}
            value={searchText}
          />
          {searchText.length > 0 && (
            <Text
              style={{ color: app_red, fontSize: 12, alignSelf: 'center' }}
              onPress={() => {
                searchText.length > 0 && apiSearchApplication();
              }}
              source={ic_search}
            >
              Search
            </Text>
          )}
        </View>

        <View style={styles.flatlistView}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={response && response['entity']}
            renderItem={(item) => (
              <View style={styles.itemList}>
                <View style={styles.itemLeftView}>
                  <Text style={[styles.leftItemText, { fontWeight: 'bold' }]}>Apl.No</Text>
                  <Text style={styles.leftItemText}>{item.item.Id}</Text>
                </View>
                <View style={styles.itemRightView}>
                  <View style={{ flex: 2 }}>
                    <Text style={styles.rightItemTextTitle}>Name</Text>
                    <Text ellipsizeMode="tail" numberOfLines={1} style={styles.rightItemText}>
                      {item.item.Name}
                    </Text>
                    <Text style={[styles.rightItemTextTitle, { marginTop: 5 }]}>Phone No</Text>
                    <Text style={styles.rightItemText}>{item.item.Phone}</Text>
                    <Text style={[styles.rightItemTextTitle, { marginTop: 5 }]}>Date</Text>
                    <Text style={styles.rightItemText}>
                      {/* {Date.parse(item.item.ApplicationDate)} */}
                      {Moment(item.item.ApplicationDate).format('DD-MM-YYYY')}
                    </Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.rightItemTextTitle}>Plan</Text>
                    <Text style={styles.rightItemText}>{item.item.Plan}</Text>
                    <Text style={[styles.rightItemTextTitle, { marginTop: 5 }]}>Status</Text>
                    {/* 1 = Pending,   2 = Approved,  3 = Rejected */}
                    <Text
                      style={[
                        styles.rightItemText,
                        item.Status == 2 ? styles.statusActive : styles.statusPending,
                      ]}
                    >
                      {item.item.Status == 1
                        ? 'Pending'
                        : item.item.Status == 2
                        ? 'Approved'
                        : 'Rejected'}
                    </Text>
                    <Text style={[styles.rightItemTextTitle, { marginTop: 5 }]}>Units</Text>
                    <Text style={styles.rightItemText}>{item.item.Units}</Text>
                  </View>
                </View>
              </View>
            )}
          />
        </View>
      </ScrollView>
      <TouchableOpacity activeOpacity={0.5} onPress={permissionRequest} style={styles.fab}>
        <Image source={ic_plus} style={styles.fabIcon} />
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    // backgroundColor: app_background,
    flex: 1,
  },
  searchBar: {
    flexDirection: 'row',
    width: '90%',
    height: 40,
    borderColor: border_gray,
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
    paddingHorizontal: 10,
    marginTop: 20,
  },
  searchIcon: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    padding: 5,
    marginRight: 5,
  },
  titleText: {
    fontSize: 26,
    paddingHorizontal: 10,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
  },
  textInput: {
    flex: 1,
  },
  flatlistView: {
    marginTop: 10,
    marginHorizontal: 10,
  },
  itemList: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    marginHorizontal: 10,
    marginBottom: 8,
  },
  itemLeftView: {
    flex: 1,
    backgroundColor: app_red,
    borderRadius: 11,
    marginRight: 5,
    paddingVertical: 10,
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftItemText: {
    textAlign: 'center',
    color: 'white',
  },
  itemRightView: {
    flex: 4,
    borderRadius: 11,
    paddingHorizontal: 15,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: item_background_gray,
  },
  rightItemTextTitle: {
    color: 'black',
    fontWeight: 'bold',
  },
  rightItemText: {
    color: 'black',
  },
  statusPending: {
    color: '#BE2533',
  },
  statusActive: {
    color: '#035817',
  },
  fabIcon: {
    width: 20,
    height: 20,
  },
  fab: {
    width: 50,
    elevation: 10,
    height: 50,
    borderRadius: 25,
    backgroundColor: app_red,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: '4%',
    right: '7%',
  },
});
