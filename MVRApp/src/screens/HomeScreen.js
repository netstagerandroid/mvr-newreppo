import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
  BackHandler,
  Alert,
} from 'react-native';

import {
  ic_search,
  ic_profile,
  ic_arrow_right,
  ic_application,
  ic_commission,
  ic_link,
  ic_key,
  ic_logout,
} from '../constants/icons';
import { app_background, app_red, border_gray } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';
import { AppBar } from '../components/AppBar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { IS_LOGGED_IN, UserId } from '../constants/appConstants';

export const HomeScreen = (props) => {
  // alert(JSON.stringify(props.route.params));
  const alertmsg = () => {
    const resetvalue = props.route.params?.resetpassword;
    // console.log(resetvalue);
    // alert(resetvalue);
    if (resetvalue) {
      showalert();
    }
  };
  const showalert = () => {
    const { navigate } = useNavigation();
    Alert.alert(
      'Please change Password',
      'Your Password and Username are same Please change it  for better security .',
      [
        {
          text: 'Cancel',
          onPress: () => {
            // alert('User cancelled');
          },
          style: 'cancel',
        },
        {
          text: 'Ok',
          onPress: async () => {
            navigate('ChangePassword');
          },
        },
        // {
        //   text: 'cancel',
        //   onPress: async () => {
        //     selectFile(type);
        //   },
        // },
      ]
    );
  };

  alertmsg();

  const { navigate } = useNavigation();
  const [isLoading, setLoading] = useState(false);
  AsyncStorage.setItem(IS_LOGGED_IN, 'true');
  // BackPressExit();

  const initScrn = async () => await AsyncStorage.setItem('screen', 'HomeScreen');

  const showLogout = () => {
    Alert.alert('Logout', 'Do you want to logout?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: async () => {
          try {
            await AsyncStorage.setItem(UserId, null)
              .then(AsyncStorage.setItem(IS_LOGGED_IN, 'false'))
              .then(props.navigation.replace('LoginScreen'));
          } catch (e) {
            console.log(e);
          }
        },
      },
    ]);
  };

  initScrn();

  return (
    <View style={styles.container}>
      <AppBar />
      <Text style={styles.titleText}>Dashboard</Text>
      <ScrollView showsHorizontalScrollIndicator={false}>
        {/* Search Bar */}
        {/* <View style={styles.searchBar}>
          <Image style={styles.searchIcon} source={ic_search} />
          <TextInput placeholder={'Search'} />
        </View> */}
        {/* Tiles menu */}
        <View style={styles.tilesContainer}>
          <View style={styles.tilesRow}>
            {/* Profile */}
            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.tilesItem}
              onPress={() => navigate('ProfileScreen')}
            >
              <Image style={styles.tileIcon} source={ic_profile} />
              <Text style={styles.tileItemText}>Profile</Text>
              <Image style={styles.arrow_right} source={ic_arrow_right} />
            </TouchableOpacity>
            {/* New Application */}
            <TouchableOpacity
              onPress={() => navigate('ApplicationsScreen')}
              activeOpacity={0.5}
              style={styles.tilesItem}
            >
              <Image style={styles.tileIcon} source={ic_application} />
              <Text style={styles.tileItemText}>Application</Text>
              <Image style={styles.arrow_right} source={ic_arrow_right} />
            </TouchableOpacity>
          </View>

          <View style={styles.tilesRow}>
            {/* Commission */}
            <TouchableOpacity
              onPress={() => navigate('CommissionScreen')}
              activeOpacity={0.5}
              style={styles.tilesItem}
            >
              <Image style={styles.tileIcon} source={ic_commission} />
              <Text style={styles.tileItemText}>Commission</Text>
              <Image style={styles.arrow_right} source={ic_arrow_right} />
            </TouchableOpacity>
            {/* Generate Link & Share */}
            <TouchableOpacity
              onPress={() => navigate('GenerateLinkScreen')}
              activeOpacity={0.5}
              style={styles.tilesItem}
            >
              <Image style={styles.tileIcon} source={ic_link} />
              <Text style={styles.tileItemText}>Generate Link & Share</Text>
              <Image style={styles.arrow_right} source={ic_arrow_right} />
            </TouchableOpacity>
          </View>

          <View style={styles.tilesRow}>
            {/* Change Password */}
            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.tilesItem}
              onPress={() => navigate('ChangePassword')}
            >
              <Image style={styles.tileIcon} source={ic_key} />
              <Text style={styles.tileItemText}>Change Password</Text>
              <Image style={styles.arrow_right} source={ic_arrow_right} />
            </TouchableOpacity>
            {/* Logout */}
            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.tilesItem}
              onPress={() => showLogout(this)}
            >
              <Image style={styles.tileIcon} source={ic_logout} />
              <Text style={styles.tileItemText}>Logout</Text>
              <Image style={styles.arrow_right} source={ic_arrow_right} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: app_background,
    flex: 1,
  },
  searchBar: {
    flexDirection: 'row',
    width: '90%',
    height: 40,
    borderColor: border_gray,
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
    paddingHorizontal: 10,
    marginTop: 20,
  },
  searchIcon: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    padding: 5,
    marginRight: 5,
  },
  titleText: {
    fontSize: 26,
    paddingHorizontal: 10,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
  },
  tilesContainer: {
    margin: 8,
    marginHorizontal: 15,
  },
  tilesRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tilesItem: {
    flex: 1,
    margin: 5,
    backgroundColor: app_red,
    padding: 14,
    justifyContent: 'space-around',
    borderRadius: 8,
    height: 150,
    minHeight: 134,
  },
  tileIcon: {
    width: 25,
    height: 25,
  },
  tileItemText: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Nunito',
    fontWeight: '100',
  },
  arrow_right: {
    width: 10,
    height: 16,
    alignSelf: 'flex-end',
  },
});
