import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Platform,
  PermissionsAndroid,
  Modal,
  Alert,
  Dimensions,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {
  app_red,
  border_gray,
  heading_text,
  forgot_password,
  radio_button,
  upload_button,
} from '../constants/colors';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { SAVE_APPLICATION, PAGE_LOAD, LOGIN } from '../constants/networkConstants';
import { AppBar } from '../components/AppBar';
import { ScrollView } from 'react-native-gesture-handler';
import { useState } from 'react';
// import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
import { Button } from '../components/Button';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';
import { useRef } from 'react';
import DocumentPicker from 'react-native-document-picker';
import { UserId } from '../constants/appConstants';
import { useEffect } from 'react';
import {
  terms_image1,
  terms_image2,
  terms_image3,
  terms_image4,
  terms_image5,
  terms_image6,
  terms_image7,
} from '../constants/icons';
import WebView from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Loader } from '../components/Loader';
import { useNavigation, NavigationAction } from '@react-navigation/native';
import { BenificiaryScreen } from './BenificiaryScreen';
import moment from 'moment';

export const NewApplicationScreen = (props) => {
  const { navigate } = useNavigation();

  const popUp = useRef();
  var planLists = [];
  var stateLists = [];
  var proofLists = [];
  var relationLists = [];
  var apprelationLists = [];

  var cityLists = [];
  const [pageLoadResponse, setPageLoadResponse] = useState();
  const [isLoading, setLoading] = useState(false);
  const [isApplicantBenificiary, setIsApplicantBenificiary] = useState(false);
  const [isMember, setIsMember] = useState(false);

  const [isTnC, setIsTnC] = useState('');
  var date = new Date();
  var today =
    date.getDate() + '/' + parseInt(date.getMonth() + 1) + '/' + parseInt(date.getFullYear() - 18);
  var lastday =
    parseInt(date.getDate() + 1) +
    '/' +
    parseInt(date.getMonth() + 1) +
    '/' +
    parseInt(date.getFullYear() - 60);
  var lastday2 =
    parseInt(date.getDate() + 1) +
    '/' +
    parseInt(date.getMonth() + 1) +
    '/' +
    parseInt(date.getFullYear() - 80);
  // var now  = "04/09/2013 15:00:00";

  const [planList, setPlanList] = useState([]);
  const [stateList, setStateList] = useState([]);
  const [districtList, setDistrictList] = useState([]);
  const [proofList, setProofList] = useState([]);
  const [relationList, setRelationList] = useState([]);
  const [apprelationList, setAppRelationList] = useState([]);

  const [plan, setPlan] = useState();
  const [depositorName, setDepositorName] = useState();
  const [communicationAddress, setcommunicationAddress] = useState();

  const [depositorLastName, setDepositorLastName] = useState();
  const [phoneNumber, setPhone] = useState();
  const [email, setEmail] = useState();
  const [dateOfBirth, setDateOfBirth] = useState();

  const [houseName, setHouseName] = useState();
  const [street, setStreet] = useState();
  const [city, setCity] = useState();
  const [postCode, setPostCode] = useState();
  const [postOffice, setPostOffice] = useState();
  const [district, setDistrict] = useState();
  const [state, setState] = useState();
  const [idNumber, setIdNumber] = useState();
  const [units, setUnits] = useState(0);
  const [nominieType, setNominieType] = useState();
  const [genderType, setgenderType] = useState();
  const [maritalStatus, setmaritalStatus] = useState();

  const [typeOfProof, setTypeOfProof] = useState();
  const [addressProof1, setAddressProof1] = useState();
  const [addressProof2, setAddressProof2] = useState();
  const [birthProof1, setBirthProof1] = useState();
  const [birthProof2, setBirthProof2] = useState();
  const [photo, setPhoto] = useState();
  const [signature, setSignature] = useState();
  const [nominieeName, setNominieeName] = useState();
  const [nominieeRelation, setNominieeRelation] = useState();
  const [nominieeDob, setNominieeDob] = useState();
  const [nominieeGuardian, setNominieeGuardian] = useState();
  const [showBenificiary, setShowBenificiary] = useState(false);
  const [nominieeList, setNomineeList] = useState([]);
  const [unitlist, setunitList] = useState(0);
  const [shouldRender, setShouldRender] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [agentid, setagentid] = useState();
  const [datediff, setdatediff] = useState();

  const reset = async () => {
    setPlan('');
    setDepositorName();
    setmaritalStatus();
    setgenderType();
    setDepositorLastName();
    setPhone();
    setEmail();
    setDateOfBirth();
    setHouseName();
    setStreet();
    setCity();
    setPostCode();
    setPostOffice();
    setDistrict();
    setState();
    setIdNumber();
    setUnits('');
    setNominieType();
    setTypeOfProof('');
    setAddressProof1();
    setAddressProof2();
    setBirthProof1();
    setBirthProof2();
    setPhoto();
    setSignature();
    setNominieeName();
    setIsMember(false);
    setIsApplicantBenificiary(false);
    setIsTnC(false);
    setNominieeRelation(0);
    setNomineeList([]);
    setunitList(0);
    setcommunicationAddress();
    // item.tdFname == null,
  };
  const pageLoadApi = async (id) => {
    setLoading(true);
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    await fetch(PAGE_LOAD + 4, {
      headers: headers,
      method: 'POST',
    })
      .then((responsea) => {
        console.log(responsea);
        return Promise.all([responsea.status, responsea.json()]);
      })
      .then((responseJson) => {
        console.log(JSON.stringify(responseJson[1]));
        setLoading(false);
        if (responseJson[0] === 200) {
          setPageLoadResponse(responseJson[1]);
        } else if (responseJson[0] === 401) {
          // AsyncStorage.clear().then(navigate('LoginScreen'));
        }
      })
      .then(() => setDropDownData())
      .catch((err) => console.log(err));
  };
  const apiCallSubmitForm = async () => {
    const body = new FormData();
    setLoading(true);
    var nomList = [];
    console.log('nomini', JSON.stringify(nominieeList));
    for (const [i, v] of nominieeList.entries()) {
      console.log(i, v);

      body.append(i + '_tdUploadPhoto', v.photo);
      body.append(i + '_tdAdressProof', v.addressProof);
      body.append(i + '_tdBirthProof', v.birthProof);
      body.append(i + '_tdAdressProof2', v.addressProof3);
      body.append(i + '_tdBirthProof2', v.birthProof3);

      delete v['addressProof'];
      delete v['photo'];
      delete v['birthProof'];
      delete v['addressProof3'];
      delete v['birthProof3'];

      nomList.push(v);
    }
    var data = {
      AgentId: agentid,
      FirstName: depositorName,
      LastName: depositorLastName,
      phoneNumber1: phoneNumber,
      phoneNumber2: phoneNumber,
      Email: email,
      DepositAmount: 10.0,
      StateId: '4',
      DistrictId: district.CityId,
      postCode: postCode,
      Address: houseName + ', ' + street + ', ' + postOffice + ' ' + city,
      Gender: genderType,
      MaritalStatus: maritalStatus,
      AddressCommunication: communicationAddress,
      PaymentStatus: 'False',
      ApplicationStatus: '1',
      DateofBirth: dateOfBirth,
      CancelReason: 'asgdsdg',
      Housename: houseName,
      AddressProofId: typeOfProof.ProofId,
      Street: street,
      PostOffice: postOffice,
      City: city,
      BuyTypeId: plan.Id,
      NomineeUnits: plan.Id == 4 ? Number(units) + unitlist : Number(units),
      IdNo: idNumber,
      NomineeFN: nominieeName,
      ChkAppBen: isApplicantBenificiary ? 1 : 0,
      ChkMember: isMember ? 'True' : 'False',
      // tdDOB: nominieeDob,
      _NomineeList: nomList,
    };

    // var dummy =
    //   '{"FirstName":"neeraj test","LastName":"n","phoneNumber1":"7777777777","phoneNumber2":"7777777777","Email":"n@gmail.com","DepositAmount":"10.00","StateId":"4","DistrictId":"55","postCode":"333333","Address":"ffffffff","PaymentStatus":"False","ApplicationStatus":"1","DateofBirth":"22/08/1997","Housename":"hhhhhh","City":"tly","BuyTypeId":"1","NomineeFN":"ttttt","_NomineeList":[]}';
    // console.log('########################');
    // console.log(dummy);
    // console.log('########################');

    console.log(JSON.stringify(data.NomineeUnits));
    console.log('########################');

    body.append('Data', JSON.stringify(data));

    body.append('Addressproof', {
      uri: addressProof1.uri,
      type: addressProof1.type,
      name: addressProof1.fileName,
    });
    body.append(
      'Addressproof2',
      addressProof2
        ? {
            uri: addressProof2?.uri,
            type: addressProof2?.type,
            name: addressProof2?.fileName,
          }
        : null
    );
    body.append('UploadPhoto', {
      uri: photo.uri,
      type: photo.type,
      name: photo.fileName,
    });
    body.append('BirthProof', {
      uri: birthProof1.uri,
      type: birthProof1.type,
      name: birthProof1.fileName,
    });
    body?.append(
      'BirthProof2',
      birthProof2
        ? {
            uri: birthProof2?.uri,
            type: birthProof2?.type,
            name: birthProof2?.fileName,
          }
        : null
    );
    body.append('Signature', {
      uri: signature.uri,
      type: signature.type,
      name: signature.fileName,
    });
    //  console.log(JSON.stringify(body.addressProof.uri));
    //  console.log(JSON.stringify(body.addressProof.uri));

    // body.append('Addressproof', addressProof1);
    // body.append('Addressproof2', addressProof2);
    // body.append('UploadPhoto', photo);
    // body.append('BirthProof', birthProof1);
    // body.append('BirthProof2', birthProof2);
    // body.append('Signature', signature);

    var headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', '*/*');

    console.log('Request :::::: ', JSON.stringify(body));

    await fetch('https://mvrapi.calicutcitybank.com/api/Applications/SaveApplication', {
      method: 'POST',
      body: body,
      headers: headers,
    })
      .then((responsea) => {
        console.log(responsea);
        return Promise.all([responsea.status, responsea.json()]);
      })
      .then((responseJson) => {
        console.log('response :::::: ', JSON.stringify(responseJson));
        setLoading(false);
        // if (responseJson[0] === 200) {

        ToastAndroid.show('Application Added Successfully ', ToastAndroid.SHORT);
        reset();
        navigate('PaymentGatewayScreen', { response: responseJson[1] });
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      })
      .finally(() => setLoading(false));
  };
  const showcam = (type) => {
    Alert.alert('Select Image', 'Choose From ', [
      {
        text: 'Cancel',
        onPress: () => {
          alert('User cancelled image picker');
        },
        style: 'cancel',
      },
      {
        text: 'Camera',
        onPress: async () => {
          captureimage(type);
        },
      },
      {
        text: 'Gallery',
        onPress: async () => {
          selectFile(type);
        },
      },
    ]);
  };
  const captureimage = async (type) => {
    var options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },

      maxWidth: 500,
      maxHeight: 500,
      quality: 0.5,
    };

    await launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        // alert('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let res = response;
        switch (type) {
          case 'AddressProof1':
            setAddressProof1(res);
            break;
          case 'AddressProof2':
            setAddressProof2(res);
            break;
          case 'BirthProof1':
            setBirthProof1(res);
            break;
          case 'BirthProof2':
            setBirthProof2(res);
            break;
          case 'Photo':
            setPhoto(res);
            break;
          case 'Signature':
            setSignature(res);
            break;
        }
        // alert('ImagePicker: ' + JSON.stringify(source));
      }
    });
  };

  const selectFile = async (type) => {
    var options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },

      maxWidth: 500,
      maxHeight: 500,
      quality: 0.5,
    };
    await launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        alert('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let res = response;
        switch (type) {
          case 'AddressProof1':
            setAddressProof1(res);
            break;
          case 'AddressProof2':
            setAddressProof2(res);
            break;
          case 'BirthProof1':
            setBirthProof1(res);
            break;
          case 'BirthProof2':
            setBirthProof2(res);
            break;
          case 'Photo':
            setPhoto(res);
            break;
          case 'Signature':
            setSignature(res);
            break;
        }
        // alert('ImagePicker: ' + JSON.stringify(source));
      }
    });
  };
  const selectFileOld = async (type) => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
        mode: 'import',
      });
      switch (type) {
        case 'AddressProof1':
          setAddressProof1(res);
          break;
        case 'AddressProof2':
          setAddressProof2(res);
          break;
        case 'BirthProof1':
          setBirthProof1(res);
          break;
        case 'BirthProof2':
          setBirthProof2(res);
          break;
        case 'Photo':
          setPhoto(res);
          break;
        case 'Signature':
          setSignature(res);
          break;
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log('Canceled');
      } else {
        console.log('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };
  // const Emailvalidate = async (email) => {
  //   console.log(email);
  //   const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  //   if (email.match(reg)) {
  //     console.log('true');
  //     return true;
  //   } else {
  //     console.log('false');

  //     return false;
  //   }
  // };

  const validate = async () => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!plan) {
      ToastAndroid.show('Select a plan', ToastAndroid.SHORT);
    } else if (!depositorName) {
      ToastAndroid.show('Enter depositor name ', ToastAndroid.SHORT);
    } else if (!depositorLastName) {
      ToastAndroid.show('Enter depositor last name ', ToastAndroid.SHORT);
    } else if (!phoneNumber) {
      ToastAndroid.show('Enter phone number ', ToastAndroid.SHORT);
    } else if (phoneNumber.length != 10) {
      ToastAndroid.show('Enter valid phone number ', ToastAndroid.SHORT);
    } else if (!email) {
      ToastAndroid.show('Enter Email ', ToastAndroid.SHORT);
    } else if (!email.match(reg)) {
      ToastAndroid.show('Enter valid Email ', ToastAndroid.SHORT);
    } else if (!dateOfBirth) {
      ToastAndroid.show('Enter  Date of birth ', ToastAndroid.SHORT);
    } else if (!houseName) {
      ToastAndroid.show('Enter house name ', ToastAndroid.SHORT);
    } else if (!street) {
      ToastAndroid.show('Enter street name ', ToastAndroid.SHORT);
    } else if (!city) {
      ToastAndroid.show('Enter city ', ToastAndroid.SHORT);
    } else if (!postCode) {
      ToastAndroid.show('Enter postcode  ', ToastAndroid.SHORT);
    } else if (postCode.length != 6) {
      ToastAndroid.show('Enter a valid postcode', ToastAndroid.SHORT);
    } else if (!postOffice) {
      ToastAndroid.show('Enter post office ', ToastAndroid.SHORT);
    }
    //  else if (!state) {
    //   ToastAndroid.show('Select state ', ToastAndroid.SHORT);
    // }
    else if (!district) {
      ToastAndroid.show('Enter district ', ToastAndroid.SHORT);
    } else if (!typeOfProof) {
      ToastAndroid.show('Select type of proof ', ToastAndroid.SHORT);
    } else if (!addressProof1) {
      ToastAndroid.show('Upload address proof ', ToastAndroid.SHORT);
    } else if (!birthProof1) {
      ToastAndroid.show('Upload birth proof ', ToastAndroid.SHORT);
    } else if (!photo) {
      ToastAndroid.show('Upload photo ', ToastAndroid.SHORT);
    } else if (!signature) {
      ToastAndroid.show('Upload signature ', ToastAndroid.SHORT);
    } else if (!idNumber) {
      ToastAndroid.show('Enter id number', ToastAndroid.SHORT);
    }
    // else if (idNumber.length  < 12) {
    //   ToastAndroid.show('Enter a valid ID Number', ToastAndroid.SHORT);
    // }
    else if (idNumber.length > 18) {
      ToastAndroid.show('Enter a valid ID Number', ToastAndroid.SHORT);
    } else if (plan?.Id == 4 && isApplicantBenificiary == true && !units) {
      ToastAndroid.show('Enter units ', ToastAndroid.SHORT);
    } else if (units.type > 0) {
      ToastAndroid.show('Enter valid units ', ToastAndroid.SHORT);
    } else if (!nominieType) {
      ToastAndroid.show('Select Nominee type ', ToastAndroid.SHORT);
    } else if (!nominieeName) {
      ToastAndroid.show('Enter nominee Name ', ToastAndroid.SHORT);
    } else if (!nominieeRelation) {
      ToastAndroid.show('Select Nominee Relation ', ToastAndroid.SHORT);
    } else if (!genderType) {
      ToastAndroid.show('Select Gender ', ToastAndroid.SHORT);
    } else if (!maritalStatus) {
      ToastAndroid.show('Select Marital Status ', ToastAndroid.SHORT);
    } else if (!communicationAddress) {
      ToastAndroid.show('Enter Communication Address ', ToastAndroid.SHORT);
    } else if (plan?.Id == 2 && isApplicantBenificiary == true && nominieeList.length < 1) {
      ToastAndroid.show('Please enter atleast two beneficiaries for this plan', ToastAndroid.SHORT);
    } else if (plan?.Id == 2 && isApplicantBenificiary == false && nominieeList.length < 2) {
      ToastAndroid.show('Please enter atleast two beneficiaries for this plan', ToastAndroid.SHORT);
    } else if (plan?.Id == 4 && nominieeList.length < 1) {
      ToastAndroid.show('Please enter atleast 1 beneficiaries for this plan', ToastAndroid.SHORT);
    }
    //  else if (nominieType == 'Minor') {
    // if (!nominieeDob) {
    //   ToastAndroid.show('Select nominee Date of birth ', ToastAndroid.SHORT);
    // } else if (!nominieeGuardian) {
    //   ToastAndroid.show('Select nominee guardian name ', ToastAndroid.SHORT);
    // }
    // }
    else if (plan?.Id == 1 && isApplicantBenificiary == true && datediff > 60) {
      ToastAndroid.show(
        'Applicant selected for beneficiary must have age below Sixty years.',
        ToastAndroid.SHORT
      );
    } else if (plan?.Id == 2 && isApplicantBenificiary == true && datediff > 60) {
      ToastAndroid.show(
        'Applicant selected for beneficiary must have age below Sixty years.',
        ToastAndroid.SHORT
      );
    } else if (plan?.Id == 2 && isApplicantBenificiary == true && units < 1 + nominieeList.length) {
      ToastAndroid.show(
        'Sorry Minimum Unit is' + ' ' + (1 + nominieeList.length),
        ToastAndroid.SHORT
      );
    } else if (plan?.Id == 2 && isApplicantBenificiary == false && units < nominieeList.length) {
      ToastAndroid.show('Sorry Minimum Unit is' + ' ' + nominieeList.length, ToastAndroid.SHORT);
    } else if (plan?.Id == 4 && isApplicantBenificiary == true && datediff > 60) {
      ToastAndroid.show(
        'Applicant selected for beneficiary must have age below Sixty years.',
        ToastAndroid.SHORT
      );
    } else if (!isTnC) {
      ToastAndroid.show('Please agree terms and conditions', ToastAndroid.SHORT);
    } else {
      // ToastAndroid.show("" +isvalidateunit , ToastAndroid.SHORT);
      // isvalidateunit();
      apiCallSubmitForm();
    }
  };

  const setDropDownData = async () => {
    await pageLoadResponse._BuyOptionList.forEach((item) => {
      var obj = {
        label: item.Name,
        value: item,
      };
      planLists.push(obj);
    });

    await pageLoadResponse._StateList.forEach((item) => {
      var obj = {
        label: item.State,
        value: item,
        key: item.StateId,
      };
      stateLists.push(obj);
    });
    await pageLoadResponse._ProofList.forEach((item) => {
      var obj = {
        label: item.Proof,
        value: item,
        key: item.ProofId,
      };
      proofLists.push(obj);
    });
    await pageLoadResponse._RelationList.forEach((item) => {
      var obj = {
        label: item.Relation,
        value: item,
        key: item.RelationshipId,
      };
      apprelationLists.push(obj);
    });
    await pageLoadResponse._CityList.forEach((item) => {
      var obj = {
        label: item.City,
        value: item,
        key: item.CityId,
      };
      cityLists.push(obj);
    });
    await pageLoadResponse._ApplicantRelation.forEach((item) => {
      var obj = {
        label: item.Relation,
        value: item,
        key: item.RelationshipId,
      };
      relationLists.push(obj);
    });
    await setDistrictList(cityLists);
    await setPlanList(planLists);
    await setStateList(stateLists);
    await setProofList(proofLists);
    await setRelationList(relationLists);
    await setAppRelationList(apprelationLists);
  };

  useEffect(() => {
    try {
      AsyncStorage.getItem(UserId)
        .then((v) => {
          setagentid(v);
          console.log('agentid', v);
          pageLoadApi(v);
        })
        .then();
    } catch (error) {}
  }, []);

  useEffect(() => {}, [nominieeList]);

  // useEffect(() => {
  //   isValidDateOfBirth();
  // });

  useEffect(() => {
    setDropDownData();
  }, [pageLoadResponse]);

  const changeunit = (text) => {
    if (isApplicantBenificiary == true) {
      setUnits(1);
      // const numericRegex = /^([0-9]{0,100})+$/;
      // if (text.length > 0) {
      //   if (numericRegex.test(text) && Number(text) >0) {
      //     setUnits(Number(text));
      //   }
      // } else {
      //   setUnits('');
      // }
    } else {
      setUnits(0);
    }
  };
  // const isvalidateunit = async() =>{
  //   if((plan?.Id == 4 || plan?.Id == 2)){
  //     if(isApplicantBenificiary == false ){
  //       return("1111",Number(units) + unitlist + nominieeList.length)
  //     }
  //     else{
  //       return("222",Number(units) + unitlist + nominieeList.length+1)

  //     }
  //   }
  //   else{
  //     return("333",Number(units) + unitlist + nominieeList.length)
  //   }
  // }
  // const isValidDateOfBirth = async () => {
  //   // getDateDiff('{dateOfBirth}', '{today}', 'y');
  //   // alert('hi');
  //   var dob = moment('30-06-1997', 'DD-MM-YYYY');
  //   var today_date = moment('07-04-2021', 'DD-MM-YYYY');
  //   var age = today_date.diff(dob, 'year');
  //   alert(age.toString);
  //   console.log(age.toString);
  //   // if (age >= 17 && age <= 60) {
  //   //   return true;
  //   // } else {
  //   //   return false;
  //   // }
  // };
  return (
    <View style={{ flex: 1 }}>
      <AppBar />
      {isLoading && <Loader />}
      <ScrollView style={{ flex: 1 }}>
        {/* Heading */}
        <View style={{ marginTop: 40 }}>
          <Text style={styles.container}>Mass Care Deposit</Text>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          // onRequestClose={() => {
          //   Alert.alert("Modal has been closed.");
          //   setModalVisible(!modalVisible);
          // }}
        >
          <View style={{ flex: 1 }}>
            <View style={{ backgroundColor: 'white', flex: 1 }}>
              <ScrollView contentContainerStyle={{ alignItems: 'center' }}>
                {plan?.Id == 1 || plan?.Id == 4 ? (
                  <View>
                    <Image
                      style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                      }}
                      source={terms_image1}
                      resizeMode="center"
                    />
                    <Image
                      style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                      }}
                      source={terms_image2}
                      resizeMode="center"
                    />
                    <Image
                      style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                      }}
                      source={terms_image3}
                      resizeMode="center"
                    />
                  </View>
                ) : (
                  <View>
                    <Image
                      style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                      }}
                      source={terms_image4}
                      resizeMode="center"
                    />
                    <Image
                      style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                      }}
                      source={terms_image5}
                      resizeMode="center"
                    />
                    <Image
                      style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                      }}
                      source={terms_image6}
                      resizeMode="center"
                    />
                    <Image
                      style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                      }}
                      source={terms_image7}
                      resizeMode="center"
                    />
                  </View>
                )}
              </ScrollView>
            </View>
          </View>
          <TouchableOpacity
            style={[styles.button, styles.buttonClose]}
            onPress={() => setModalVisible(!modalVisible)}
          >
            <Text style={styles.textStyle}>Hide Terms and Conditions</Text>
          </TouchableOpacity>
          {/* <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Hello World!</Text>
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Hide Modal</Text>
            </TouchableOpacity>
          </View>
        </View> */}
        </Modal>

        <Modal
          style={{ width: '80%' }}
          animationType="slide"
          transparent={false}
          visible={showBenificiary}
          onRequestClose={() => {
            setShowBenificiary(false);
            ToastAndroid.show('Cancelled', ToastAndroid.SHORT);
          }}
        >
          <BenificiaryScreen
            planId={plan}
            relationList={relationList}
            districts={districtList}
            states={stateList}
            proofList={proofList}
            data={nominieeList}
            close={async (nominiees) => {
              await setShowBenificiary(false);
              await setNomineeList([]);
              await setNomineeList(nominiees);
              {
                plan?.Id == 2 &&
                  (isApplicantBenificiary == true
                    ? setUnits(1 + nominiees.length)
                    : setUnits(0 + nominiees.length));
              }

              // var totalunit = [];
              // totalunit = setNomineeList;
              console.log(JSON.stringify(nominiees), '++++++++++++++++++++++==');
              var count = 0;
              nominiees.forEach((element) => {
                console.log(JSON.stringify(element), 'oooooooooooooooooooooooooooooo');

                count = count + Number(element.tdUnits);
                setunitList(count);
              });

              // await alert(nominieeList.length + '' + nominiees.length);
            }}
          />
        </Modal>

        {/* Input Fields */}
        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
          <Text style={styles.titleText}>Plan :</Text>
          <View style={styles.pickerView}>
            <RNPickerSelect
              // style={styles.picker}
              placeholder={{ label: 'select plan ', value: null }}
              value={plan}
              onValueChange={(value) => {
                setPlan(value);
                console.log('+++++', value);
                if (value?.Id == 1) {
                  setUnits(1);
                } else {
                  setUnits('');
                }
                if (value?.Id == 1) {
                  setIsApplicantBenificiary(true);
                } else {
                  setUnits('');
                }
              }}
              items={planList}
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 20,
                  right: 10,
                  color: 'red',
                },
                placeholder: {
                  color: 'black',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
            />
          </View>

          {/* Depositor name */}

          <Text style={styles.titleText}>
            Depositor Name <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={depositorName}
              placeholder="First Name"
              onChangeText={(text) => {
                setDepositorName(text);
              }}
            />
          </View>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={depositorLastName}
              placeholder="Last Name"
              onChangeText={(text) => {
                setDepositorLastName(text);
              }}
            />
          </View>
          {/* Phone Number */}
          <Text style={styles.titleText}>
            Phone Number <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={phoneNumber}
              placeholder="Number"
              keyboardType={'numeric'}
              onChangeText={(text) => {
                setPhone(text);
              }}
            />
          </View>
          {/* Email */}
          <Text style={styles.titleText}>
            Email <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={email}
              placeholder="E-mail"
              onChangeText={(text) => {
                setEmail(text);
              }}
              keyboardType={'email-address'}
            />
          </View>
          {/* DOB */}
          <Text style={styles.titleText}>
            Date Of Birth <Text style={{ color: app_red }}>*</Text>
          </Text>
          {isApplicantBenificiary == true || plan?.Id == 1 ? (
            <View style={{ width: '80%' }}>
              <View style={{ alignItems: 'flex-start' }}>
                <DatePicker
                  mode="date"
                  date={dateOfBirth}
                  placeholder="DD/MM/YYYY"
                  format="DD/MM/YYYY"
                  minDate={lastday}
                  maxDate={today}
                  initialFocusedDate={false}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      width: 0,
                      height: 0,
                      position: 'absolute',
                    },
                    dateInput: {
                      marginBottom: 10,
                      height: 45,
                      borderRadius: 10,
                      borderWidth: 1,
                      marginTop: 8,
                      color: heading_text,
                      borderColor: border_gray,
                    },
                  }}
                  onDateChange={(date) => {
                    setDateOfBirth(date);
                    var then = date;
                    var now = new Date();
                    var ms = moment(now, 'DD/MM/YYYY HH:mm:ss').diff(
                      moment(then, 'DD/MM/YYYY HH:mm:ss')
                    );
                    var d = moment.duration(ms).asYears();
                    var roundedNumber = Math.round(d * 10) / 10;
                    setdatediff(roundedNumber);

                    // var date = '1475235770601';
                    // var ds = (d*2.7777777777778E-7 )/24;
                    // var dsw = ds.toLocaleString();
                    // var s = ds/365;
                    console.log('' + roundedNumber);
                    var then1 = lastday;
                    var now2 = date;
                    var ms1 = moment(now2, 'DD/MM/YYYY HH:mm:ss').isBefore(
                      moment(then1, 'DD/MM/YYYY HH:mm:ss')
                    );
                    if (ms1) {
                      alert(`This date of birth is not supported. Select after ${lastday}`);
                    } else {
                      setDateOfBirth(date);
                    }
                    console.log('' + ms1);
                  }}
                />
              </View>
            </View>
          ) : (
            <View style={{ width: '80%' }}>
              <View style={{ alignItems: 'flex-start' }}>
                <DatePicker
                  mode="date"
                  date={dateOfBirth}
                  placeholder="DD/MM/YYYY"
                  format="DD/MM/YYYY"
                  minDate={lastday2}
                  maxDate={today}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      width: 0,
                      height: 0,
                      position: 'absolute',
                    },
                    dateInput: {
                      marginBottom: 10,
                      height: 45,
                      borderRadius: 10,
                      borderWidth: 1,
                      marginTop: 8,
                      color: heading_text,
                      borderColor: border_gray,
                    },
                  }}
                  onDateChange={(date) => {
                    setDateOfBirth(date);
                    var then = date;
                    var now = new Date();
                    var ms = moment(now, 'DD/MM/YYYY HH:mm:ss').diff(
                      moment(then, 'DD/MM/YYYY HH:mm:ss')
                    );
                    var d = moment.duration(ms).asYears();
                    var roundedNumber = Math.round(d * 10) / 10;
                    setdatediff(roundedNumber);

                    // var date = '1475235770601';
                    // var ds = (d*2.7777777777778E-7 )/24;
                    // var dsw = ds.toLocaleString();
                    // var s = ds/365;
                    console.log('' + roundedNumber);
                    var then1 = lastday2;
                    var now2 = date;
                    var ms1 = moment(now2, 'DD/MM/YYYY HH:mm:ss').isBefore(
                      moment(then1, 'DD/MM/YYYY HH:mm:ss')
                    );
                    if (ms1) {
                      alert(`This date of birth is not supported. Select after ${lastday2}`);
                    } else {
                      setDateOfBirth(date);
                    }
                    console.log('' + ms1);
                  }}
                />
              </View>
            </View>
          )}
          <Text style={styles.titleText}>
            Gender <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={{ width: '90%', marginBottom: 10 }}>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setgenderType('male');
                }}
                style={styles.radioButtonOuter}
              >
                {genderType === 'male' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Male </Text>
            </View>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setgenderType('female');
                }}
                style={styles.radioButtonOuter}
              >
                {genderType === 'female' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Female </Text>
            </View>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setgenderType('other');
                }}
                style={styles.radioButtonOuter}
              >
                {genderType === 'other' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Other </Text>
            </View>
          </View>

          <Text style={styles.titleText}>
            Marital Status <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={{ width: '90%', marginBottom: 10 }}>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setmaritalStatus('single');
                }}
                style={styles.radioButtonOuter}
              >
                {maritalStatus === 'single' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Single </Text>
            </View>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setmaritalStatus('married');
                }}
                style={styles.radioButtonOuter}
              >
                {maritalStatus === 'married' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Married </Text>
            </View>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setmaritalStatus('widowed');
                }}
                style={styles.radioButtonOuter}
              >
                {maritalStatus === 'widowed' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Widowed </Text>
            </View>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setmaritalStatus('divorced');
                }}
                style={styles.radioButtonOuter}
              >
                {maritalStatus === 'divorced' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Divorced </Text>
            </View>
          </View>
          {/*House Name/Street/State/District/City/Post Code/Office */}

          <Text style={styles.titleText}>
            House Name <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={houseName}
              onChangeText={(text) => {
                setHouseName(text);
              }}
              placeholder="House Name"
            />
          </View>

          <Text style={styles.titleText}>
            Street <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={street}
              onChangeText={(text) => {
                setStreet(text);
              }}
              placeholder="Street"
            />
          </View>

          <Text style={styles.titleText}>
            City <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={city}
              onChangeText={(text) => {
                setCity(text);
              }}
              placeholder="City"
            />
          </View>

          <Text style={styles.titleText}>
            Post Code <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={{ width: '80%' }}>
            <View style={styles.textBoxDOB3}>
              <TextInput
                placeholder="Post Code"
                value={postCode}
                onChangeText={(text) => {
                  setPostCode(text);
                }}
                keyboardType={'numeric'}
              />
            </View>
          </View>

          <Text style={styles.titleText}>
            Post Office <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={postOffice}
              onChangeText={(text) => {
                setPostOffice(text);
              }}
              placeholder="Post Office"
            />
          </View>

          <Text style={styles.titleText}>
            State <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.pickerView}>
            <RNPickerSelect
              value={state}
              // style={styles.picker}
              disabled={true}
              placeholder={{ label: 'Kerala', value: '4' }}
              onValueChange={(value) => setState(value)}
              items={stateList}
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 20,
                  right: 10,
                  color: 'red',
                },
                placeholder: {
                  color: 'black',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
            />
          </View>

          <Text style={styles.titleText}>
            District <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.pickerView}>
            <RNPickerSelect
              // style={styles.picker}
              value={district}
              useNativeAndroidPickerStyle={true}
              placeholder={{ label: 'Select District', value: null }}
              onValueChange={(value) => setDistrict(value)}
              items={districtList}
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 20,
                  right: 10,
                },
                placeholder: {
                  color: 'black',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
            />
          </View>

          {/*Proof  */}
          <Text style={styles.titleText}>
            Type of Proof <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.pickerView}>
            <RNPickerSelect
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 20,
                  right: 10,
                  color: 'red',
                },

                placeholder: {
                  color: 'black',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              useNativeAndroidPickerStyle={true}
              // style={styles.picker}
              value={typeOfProof}
              placeholder={{ label: 'Select Address Proof', value: null }}
              onValueChange={(value) => setTypeOfProof(value)}
              items={proofList}
            />
          </View>
          {/* Upload proof */}
          <Text style={styles.titleText}>
            Upload Address proof <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={{ flexDirection: 'column' }}>
            <View style={styles.textBox}>
              <TouchableOpacity onPress={() => showcam('AddressProof1')} style={styles.touchable}>
                <Text style={styles.upLoadTouchable}>Upload Files</Text>
                <Text numberOfLines={2} ellipsizeMode={'tail'} style={styles.fileName}>
                  {addressProof1 ? 'selected' : 'Choose a file'}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.textBox}>
              <TouchableOpacity onPress={() => showcam('AddressProof2')} style={styles.touchable}>
                <Text style={styles.upLoadTouchable}>Upload Files</Text>
                <Text numberOfLines={2} ellipsizeMode={'tail'} style={styles.fileName}>
                  {addressProof2 ? 'selected' : 'Choose a file'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.titleText}>
            Upload Birth Proof <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={{ flexDirection: 'column' }}>
            <View style={styles.textBox}>
              <TouchableOpacity onPress={() => showcam('BirthProof1')} style={styles.touchable}>
                <Text style={styles.upLoadTouchable}>Upload Files</Text>
                <Text numberOfLines={2} ellipsizeMode={'tail'} style={styles.fileName}>
                  {birthProof1 ? 'selected' : 'Choose a file'}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.textBox}>
              <TouchableOpacity onPress={() => showcam('BirthProof2')} style={styles.touchable}>
                <Text style={styles.upLoadTouchable}>Upload Files</Text>
                <Text numberOfLines={2} ellipsizeMode={'tail'} style={styles.fileName}>
                  {birthProof2 ? 'selected' : 'Choose a file'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.titleText}>
            Upload Photo <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TouchableOpacity onPress={() => showcam('Photo')} style={styles.touchable}>
              <Text style={styles.upLoadTouchable}>Upload Files</Text>
              <Text numberOfLines={2} ellipsizeMode={'tail'} style={styles.fileName}>
                {photo ? 'selected' : 'Choose a file'}
              </Text>
            </TouchableOpacity>
          </View>

          <Text style={styles.titleText}>
            Upload Signature <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={{ flexDirection: 'column' }}>
            <View style={styles.textBox}>
              <TouchableOpacity onPress={() => showcam('Signature')} style={styles.touchable}>
                <Text style={styles.upLoadTouchable}>Upload Files</Text>
                <Text numberOfLines={2} ellipsizeMode={'tail'} style={styles.fileName}>
                  {signature ? 'selected' : 'Choose a file'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.titleText}>
            Address for Communication <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBoxs}>
            <TextInput
              multiline={true}
              style={styles.input}
              value={communicationAddress}
              placeholder="Type Address"
              onChangeText={(text) => {
                setcommunicationAddress(text);
              }}
            />
          </View>
          {/* ID Number */}
          <Text style={styles.titleText}>
            ID Proof No: <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              value={idNumber}
              onChangeText={(text) => {
                setIdNumber(text);
              }}
              placeholder="Enter ID Number"
            />
          </View>
          {/*Check1 */}

          {/* <View style={styles.CheckBoxView}></View> */}
          {((plan?.Id == 4 && isApplicantBenificiary == true) ||
            plan?.Id == 2 ||
            plan?.Id == 1) && (
            <Text style={styles.titleText}>
              Units <Text style={{ color: app_red }}>*</Text>
            </Text>
          )}

          {((plan?.Id == 4 && isApplicantBenificiary == true) ||
            plan?.Id == 2 ||
            plan?.Id == 1) && (
            <View style={{ width: '80%' }}>
              <View style={styles.textBoxDOB3}>
                <TextInput
                  style={{ flexGrow: 1 }}
                  // onChangeText={changeunit}
                  onChangeText={(text) => {
                    setUnits(text);
                    // if (isApplicantBenificiary == true) {
                    //   if (text < 1 + nominieeList.length) {
                    //   }
                    //   alert('minimum unit is' + (1 + nominieeList.length));
                    // } else {
                    //   setUnits(text);
                    // }
                  }}
                  keyboardType={'numeric'}
                  value={'' + units}
                />
              </View>
            </View>
          )}

          {/* Check 2 */}
          <View style={styles.CheckBoxView}>
            <View>
              <CheckBox
                style={styles.checkBox}
                value={isMember}
                onValueChange={(v) => setIsMember(v)}
              />
            </View>
            <View>
              <Text style={styles.checkBoxText}>
                I am a member/customer of the Calicut City Service Bank {'\n'}(Note: If not a
                registered member, Rs. 16 will be charged for the Membership)
              </Text>
            </View>
          </View>
          {plan?.Id == 1 ? (
            <View style={styles.CheckBoxView}>
              <View>
                <CheckBox
                  disabled
                  style={styles.checkBox}
                  value={true}
                  //  value={isApplicantBenificiary }
                  onValueChange={(v) => setIsApplicantBenificiary(v)}
                />
              </View>
              <View>
                <Text style={styles.checkBoxText1}>Add applicant as Beneficiary</Text>
              </View>
            </View>
          ) : (
            <View style={styles.CheckBoxView}>
              <View>
                <CheckBox
                  style={styles.checkBox}
                  value={isApplicantBenificiary}
                  onValueChange={(v) => {
                    v == true ? setUnits(Number(units) + 1) : setUnits(nominieeList.length);
                    setIsApplicantBenificiary(v);
                    if (plan.Id == 4) {
                      v == true ? setUnits(0) : setUnits(0);
                      setIsApplicantBenificiary(v);
                    }
                  }}
                />
              </View>
              <View>
                <Text style={styles.checkBoxText1}>Add applicant as Beneficiary</Text>
              </View>
            </View>
          )}

          {/* Radio Button */}
          <Text style={styles.titleText}>
            Nominee <Text style={{ color: app_red }}>*</Text>
          </Text>
          <View style={{ width: '90%', marginBottom: 10 }}>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setNominieType('Major');
                }}
                style={styles.radioButtonOuter}
              >
                {nominieType === 'Major' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Major </Text>
            </View>
            <View style={styles.radioButtonConatiner}>
              <TouchableOpacity
                onPress={() => {
                  setNominieType('Minor');
                }}
                style={styles.radioButtonOuter}
              >
                {nominieType === 'Minor' && <View style={styles.radioButtonInner} />}
              </TouchableOpacity>
              <Text> Minor </Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', width: '80%' }}>
            <View style={styles.textBoxDOB3}>
              <TextInput
                value={nominieeName}
                onChangeText={(text) => {
                  setNominieeName(text);
                }}
                style={styles.input}
                placeholder={'First Name'}
              />
            </View>

            <View style={styles.pickerView1}>
              <RNPickerSelect
                style={{
                  ...pickerSelectStyles,
                  iconContainer: {
                    top: 20,
                    right: 10,
                    color: 'red',
                  },
                  placeholder: {
                    color: 'black',
                    fontSize: 12,
                    fontWeight: 'bold',
                  },
                }}
                useNativeAndroidPickerStyle={true}
                // style={styles.picker}
                placeholder={{ label: 'Select Relationship', value: null }}
                value={nominieeRelation}
                onValueChange={(value) => setNominieeRelation(value)}
                items={apprelationList}
              />
            </View>
          </View>
          {/* {nominieType === 'Minor' && ( */}
          {/* <View style={{ width: '80%' }}> */}
          {/* <View style={{ alignItems: 'flex-start' }}> */}
          {/* <DatePicker
                  mode="date"
                  date={nominieeDob}
                  placeholder="Date of birth"
                  format="DD/MM/YYYY"
                  maxDate={today}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      width: 0,
                      height: 0,
                      position: 'absolute',
                    },
                    dateInput: {
                      marginBottom: 10,
                      height: 45,
                      borderRadius: 10,
                      borderWidth: 1,
                      marginTop: 8,
                      color: heading_text,
                      borderColor: border_gray,
                    },
                  }}
                  onDateChange={(date) => {
                    setNominieeDob(date);
                  }}
                /> */}
          {/* <View style={[styles.textBox, { width: '100%', marginTop: 8 }]}>
                  <TextInput
                    style={styles.input}
                    onChangeText={(text) => setNominieeGuardian(text)}
                    placeholder={'Guardian Name'}
                  />
                </View> */}
          {/* </View> */}
          {/* </View> */}
          {/* )} */}
          {(plan?.Id == 4 || plan?.Id == 2) && (
            <TouchableOpacity
              onPress={() => {
                setShowBenificiary(true);
              }}
            >
              <Text style={styles.clearAll}>Add Beneficiary</Text>
            </TouchableOpacity>
          )}
          {nominieeList && nominieeList.length > 0 && (
            <Text style={styles.titleText}>Added Beneficiaries</Text>
          )}
          {nominieeList.map((item) => (
            <View
              key={item}
              style={{
                width: '80%',
                alignSelf: 'center',
                paddingVertical: 8,
                flexDirection: 'row',
                flex: 1,
                borderRadius: 5,
                marginBottom: 8,
                borderColor: 'black',
                borderWidth: 1,
                marginHorizontal: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <Text style={{ marginHorizontal: 10, fontSize: 18 }}>{item.tdFname}</Text>
            </View>
          ))}

          {/* Total Amount */}
          {/* <View>
            <Text style={styles.titleText}>Total Units: {Number(units) + unitlist}</Text>
          </View> */}

          {/* {plan?.Id == 4 || plan?.Id == 2 ? (
            <View>
              {isApplicantBenificiary == false ? (
                <Text style={styles.titleText}>Total Units: {Number(units)}</Text>
              ) : (
                <Text style={styles.titleText}>Total Units: {Number(units)}</Text>
              )}
            </View>
          ) : (
            <View>
              <Text style={styles.titleText}>Total Units: {Number(units)}</Text>
            </View>
          )} */}

          {plan?.Id == 4 ? (
            <View>
              <Text style={styles.titleText}>Total Units: {Number(units) + unitlist}</Text>
            </View>
          ) : (
            <View>
              {Number(units) > 1 + nominieeList.length ? (
                <View>
                  {plan?.Id == 2 ? (
                    <View>
                      <Text style={styles.titleText}>Total Units: {Number(units)}</Text>
                    </View>
                  ) : (
                    <View>
                      <Text style={styles.titleText}>Total Units: {Number(units)}</Text>
                    </View>
                  )}
                </View>
              ) : (
                <View>
                  {plan?.Id == 2 ? (
                    <View>
                      {isApplicantBenificiary == true ? (
                        <Text style={styles.titleText}>Total Units: {1 + nominieeList.length}</Text>
                      ) : (
                        <Text style={styles.titleText}>Total Units: {nominieeList.length}</Text>
                      )}
                    </View>
                  ) : (
                    <View>
                      <Text style={styles.titleText}>Total Units: {Number(units)}</Text>
                    </View>
                  )}
                </View>
              )}
            </View>
          )}
          {plan?.Id == 4 ? (
            <View>
              {isMember == false ? (
                <Text style={styles.titleText}>
                  Total Amount :{' '}
                  {(parseInt(units ? units : 0) + unitlist) * parseInt(plan?.Amount)
                    ? (parseInt(units ? units : 0) + unitlist) * parseInt(plan?.Amount) + 16
                    : 0}
                </Text>
              ) : (
                <Text style={styles.titleText}>
                  Total Amount:{' '}
                  {(parseInt(units ? units : 0) + unitlist) * parseInt(plan?.Amount)
                    ? (parseInt(units ? units : 0) + unitlist) * parseInt(plan?.Amount)
                    : 0}
                </Text>
              )}
            </View>
          ) : (
            <View>
              {Number(units) > 1 + nominieeList.length ? (
                <View>
                  {isMember == false ? (
                    <Text style={styles.titleText}>
                      Total Amount :{' '}
                      {parseInt(units ? units : 0) * parseInt(plan?.Amount)
                        ? parseInt(units ? units : 0) * parseInt(plan?.Amount) + 16
                        : 0}
                    </Text>
                  ) : (
                    <Text style={styles.titleText}>
                      Total Amount:{' '}
                      {parseInt(units ? units : 0) * parseInt(plan?.Amount)
                        ? parseInt(units ? units : 0) * parseInt(plan?.Amount)
                        : 0}
                    </Text>
                  )}
                </View>
              ) : (
                <View>
                  {isApplicantBenificiary == true ? (
                    <View>
                      {isMember == false ? (
                        <Text style={styles.titleText}>
                          Total Amount :{' '}
                          {(1 + parseInt(nominieeList.length)) * parseInt(plan?.Amount)
                            ? (1 + parseInt(nominieeList.length)) * parseInt(plan?.Amount) + 16
                            : 0}
                        </Text>
                      ) : (
                        <Text style={styles.titleText}>
                          Total Amount:{' '}
                          {(1 + parseInt(nominieeList.length)) * parseInt(plan?.Amount)
                            ? (1 + parseInt(nominieeList.length)) * parseInt(plan?.Amount)
                            : 0}
                        </Text>
                      )}
                    </View>
                  ) : (
                    <View>
                      {isMember == false ? (
                        <Text style={styles.titleText}>
                          Total Amount :{' '}
                          {parseInt(nominieeList.length) * parseInt(plan?.Amount)
                            ? parseInt(nominieeList.length) * parseInt(plan?.Amount) + 16
                            : 0}
                        </Text>
                      ) : (
                        <Text style={styles.titleText}>
                          Total Amount:{' '}
                          {parseInt(nominieeList.length) * parseInt(plan?.Amount)
                            ? parseInt(nominieeList.length) * parseInt(plan?.Amount)
                            : 0}
                        </Text>
                      )}
                    </View>
                  )}
                </View>
              )}
            </View>
          )}

          {/* {(plan?.Id == 4 || plan?.Id == 2) && isApplicantBenificiary == true ? (
            <View>
              {isMember == false ? (
                <Text style={styles.titleText}>
                  Total Amount :{' '}
                  {(parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                  parseInt(plan?.Amount)
                    ? (parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                        parseInt(plan?.Amount) +
                      16
                    : 0}
                </Text>
              ) : (
                <Text style={styles.titleText}>
                  Total Amount:{' '}
                  {(parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                  parseInt(plan?.Amount)
                    ? (parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                      parseInt(plan?.Amount)
                    : 0}
                </Text>
              )}
            </View>
          ) : (
            <View>
              {isMember == false ? (
                <Text style={styles.titleText}>
                  Total Amount:{' '}
                  {(parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                  parseInt(plan?.Amount)
                    ? (parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                        parseInt(plan?.Amount) +
                      16
                    : 0}
                </Text>
              ) : (
                <Text style={styles.titleText}>
                  Total Amount:{' '}
                  {(parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                  parseInt(plan?.Amount)
                    ? (parseInt(units ? units : 0) + parseInt(nominieeList.length)) *
                      parseInt(plan?.Amount)
                    : 0}
                </Text>
              )}
            </View>
          )} */}

          {/* Check 3 */}
          <TouchableOpacity onPress={() => setModalVisible(true)}>
            <View style={[styles.CheckBoxView, { alignItems: 'center' }]}>
              <CheckBox style={styles.checkBox} value={isTnC} onValueChange={(v) => setIsTnC(v)} />
              <Text
                style={[styles.checkBoxText, { color: 'blue', textDecorationLine: 'underline' }]}
              >
                I Agree With The Terms And Conditions
              </Text>
            </View>
          </TouchableOpacity>

          {/* Submit */}
          <Button
            text="Submit"
            width={{ width: '80%', marginBottom: 120 }}
            // onPress={reset}
            onPress={() => validate()}

            // onPress={apiCallSubmitForm}
          />
          {/* Clear All */}
          <View></View>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    fontSize: 26,
    color: heading_text,
    borderColor: heading_text,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  textBox: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  textBoxs: {
    justifyContent: 'flex-start',
    marginBottom: 10,
    alignItems: 'center',
    width: '80%',
    height: '5%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  input: {
    flexGrow: 1,
  },
  titleText: {
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
    marginTop: 10,
    fontWeight: 'bold',
    marginBottom: 10,
    marginLeft: 35,
  },
  textBoxDOB: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'center',
    width: '30%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
    marginLeft: '20%',
  },
  arrowIcon: {
    width: 15,
    height: 10,
  },
  CheckBoxView: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginTop: 10,
    width: '90%',
  },
  checkBox: {
    alignSelf: 'center',
  },
  textBoxDOB1: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'center',
    width: '45%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
    marginLeft: 6,
    marginRight: '20%',
  },
  clearAll: {
    textAlign: 'center',
    fontSize: 16,
    color: forgot_password,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginTop: 20,
    marginBottom: 20,
  },
  picker: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
    flexGrow: 1,
  },
  pickerView: {
    width: '80%',
    color: heading_text,
    borderColor: border_gray,
    borderRadius: 10,
    borderWidth: 1,
  },
  pickerView1: {
    width: '65%',
    height: '84%',
    color: heading_text,
    borderColor: border_gray,
    borderRadius: 10,
    borderWidth: 1,
    marginLeft: 5,
  },
  radioButton: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBoxDOB3: {
    marginBottom: 10,
    width: '36%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  textBoxDOB4: {
    marginBottom: 10,
    width: '26%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
    marginRight: 30,
  },
  radioClick: {
    height: 15,
    width: 15,
    borderRadius: 7.5,
    borderWidth: 3,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: radio_button,
    borderColor: radio_button,
  },
  radioContainer: {
    flexDirection: 'row',
    width: '50%',
    marginBottom: 15,
    marginTop: 15,
    marginRight: 80,
    marginLeft: -25,
  },
  radioText: {
    marginLeft: 5,
  },
  checkBoxText: {
    fontSize: 12,
  },
  checkBoxText1: {
    fontSize: 12,
    marginTop: 5,
  },
  uploadView: {
    width: '90%',
    paddingHorizontal: 10,
  },
  upLoadTouchable: {
    backgroundColor: upload_button,
    borderWidth: 0,
    borderColor: border_gray,
    paddingVertical: 8,
    paddingHorizontal: 13,
    alignSelf: 'center',
    borderRadius: 10,
  },
  fileName: {
    textAlign: 'right',
    width: '60%',
  },
  touchable: {
    height: 40,
    width: 130,
    marginTop: 10,
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 8,
    flexDirection: 'row',
  },
  radioButtonInner: {
    width: 12,
    height: 12,
    borderRadius: 8,
    backgroundColor: app_red,
    margin: 8,
  },
  radioButtonOuter: {
    width: 18,
    justifyContent: 'center',
    alignItems: 'center',
    height: 18,
    borderRadius: 10,
    marginRight: 5,
    borderColor: '#000',
    borderWidth: 1,
  },
  radioButtonConatiner: {
    flexDirection: 'row',
    margin: 8,
    alignSelf: 'flex-start',
    marginHorizontal: '10%',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'black',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
// android:name="android.permission.READ_EXTERNAL_STORAGE"
