import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { splash_logo } from '../constants/icons';
import { app_red } from '../constants/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { IS_LOGGED_IN } from '../constants/appConstants';

export const SplashScreen = (props) => {
  setTimeout(async () => {
    try {
      await AsyncStorage.getItem(IS_LOGGED_IN).then((value) => {
        if (value) {
          props.navigation.replace('DrawerNavigator');
        } else {
          props.navigation.replace('LoginScreen');
        }
      });
    } catch (error) {}
  }, 3000);

  return (
    <View style={styles.mainConatainer}>
      <Image style={styles.image} source={splash_logo} />
    </View>
  );
};
const styles = StyleSheet.create({
  mainConatainer: {
    backgroundColor: app_red,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  image: {
    width: '60%',
    height: '60%',
  },
});
