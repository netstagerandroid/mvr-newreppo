import React from 'react';
import { useNavigation } from '@react-navigation/native';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
  AppRegistry,
  ToastAndroid,
} from 'react-native';
import { border_gray, heading_text } from '../constants/colors';
import { AppBar } from '../components/AppBar';
import { Button } from '../components/Button';
import { Loader } from '../components/Loader';
import { ic_edit } from '../constants/icons';
import { ScrollView } from 'react-native-gesture-handler';
import { useState, useEffect } from 'react';
import { GET_PROFILE } from '../constants/networkConstants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UserId } from '../constants/appConstants';

export const ProfileScreen = (props) => {
  const { navigate } = useNavigation();
  const [userId, setUserId] = useState();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [mobile, setMobile] = useState();
  const [address, setAddress] = useState();
  const [response, setResponse] = useState();
  const [isLoading, setLoading] = useState(true);

  const getLogin = async () => {
    let loginData = '';
    try {
      loginData = await AsyncStorage.getItem('loginData').then((value) => {
        getApiProfile(value);
        // setUserId(value);
      });
    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }
    return loginData;
  };

  const initScrn = async () => await AsyncStorage.setItem('screen', 'ProfileScreen');
  initScrn();

  // const apiCallUpdateProfile = async () => {
  //   var headers = new Headers();
  //   headers.append('content-Type', 'application/json');
  //   headers.append('Accept', 'application/json');
  //   setLoading(true);
  //   var body = {
  //     UserId: userId,
  //     FirstName: firstName,
  //     LastName: lastName,
  //     Mobile: mobile,
  //     Email: email,
  //     Address: address,
  //   };
  //   await fetch('http://mvrapi.netstager.com/api/Login/UserChangeInfo', {
  //     method: 'POST',
  //     headers: headers,
  //     body: JSON.stringify(body),
  //   })
  //     .then((responsel) => responsel.json())
  //     .then((responseJson) => {
  //       console.log(responseJson);
  //       ToastAndroid.show(responseJson.Message, ToastAndroid.SHORT);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //       ToastAndroid.show('Oops... something went wrong!', ToastAndroid.SHORT);
  //     })
  //     .finally(() => setLoading(false));
  // };

  const getApiProfile = async (id) => {
    console.log(id);
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    setLoading(true);

    await fetch(GET_PROFILE + id, {
      headers: headers,
    })
      .then((resp) => {
        const statusCode = resp.status;
        const data = resp.json();
        return Promise.all([statusCode, data]);
      })
      .then((res, responseJson) => {
        setLoading(false);
        console.log(res, responseJson);
        if (res[0] === 200) {
          if (res[1].IsSuccess) {
            setResponse(res[1]);
            try {
              const items = [
                ['name', res[1].FirstName],
                ['email', res[1].Email],
              ];
              AsyncStorage.multiSet(items);
            } catch (err) {
              console.log(err);
            }
          } else {
            ToastAndroid.show(res[1].Message, ToastAndroid.SHORT);
          }
        } else {
          console.log(res);
          async () =>
            await AsyncStorage.clear().then(() => {
              navigate('LoginScreen');
              ToastAndroid.show('Please login again! ', ToastAndroid.SHORT);
            });
        }
      })
      .catch((error) => {
        console.error(error);
        return { name: 'network error', description: '' };
      })
      .finally(() => setLoading(false));
  };

  // const isValidate = async () => {
  //   if (firstName.length == 0) {
  //     ToastAndroid.show('Enter valid Firstame', ToastAndroid.SHORT);
  //   } else if (lastName.length == 0) {
  //     ToastAndroid.show('Enter valid Lastname', ToastAndroid.SHORT);
  //   } else if (mobile.length != 10 || mobile.length != 12) {
  //     ToastAndroid.show('Enter valid Mobile number', ToastAndroid.SHORT);
  //   } else if (email.length == 0) {
  //     ToastAndroid.show('Enter valid email', ToastAndroid.SHORT);
  //   } else if (address.length == 0) {
  //     ToastAndroid.show('Enter valid address', ToastAndroid.SHORT);
  //   } else {
  //     apiCallUpdateProfile();
  //   }
  // };

  useEffect(() => {
    AsyncStorage.getItem(UserId).then((id) => {
      getApiProfile(id);
    });
  }, []);

  useEffect(() => {
    setFirstName(response?.entity.FirstName);
    setLastName(response?.entity.LastName);
    setMobile(response?.entity.Mobile);
    setEmail(response?.entity.Email);
    setAddress(response?.entity.Address);
  }, [response]);

  return (
    <View style={{ flex: 1 }}>
      <AppBar />
      {/* Heading */}
      {isLoading && <Loader />}
      <ScrollView style={{ flex: 1 }}>
        <View style={{ marginTop: 40 }}>
          <Text style={styles.container}>Profile</Text>
        </View>
        {/* Input fields */}
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 30,
          }}
        >
          {/* Name */}
          <View style={styles.viewSmall}>
            <TextInput
              style={styles.inputSmall}
              value={firstName}
              onChangeText={(text) => setFirstName(text)}
              placeholder={'First Name'}
              editable={false}
            />
            {/* <Image style={styles.editIcon} source={ic_edit} /> */}
          </View>
          <View style={styles.viewSmall}>
            <TextInput
              style={styles.inputSmall}
              onChangeText={(text) => setLastName(text)}
              placeholder={'Last Name'}
              value={lastName}
              editable={false}
            />
            {/* <Image style={styles.editIcon} source={ic_edit} /> */}
          </View>
          {/* Mobile */}
          <View style={styles.viewSmall}>
            <TextInput
              style={styles.inputSmall}
              keyboardType={'numeric'}
              onChangeText={(text) => setMobile(text)}
              value={mobile}
              placeholder={'Mobile'}
              editable={false}
            />
            {/* <Image style={styles.editIcon} source={ic_edit} /> */}
          </View>
          {/* Email */}
          <View style={styles.viewSmall}>
            <TextInput
              style={styles.inputSmall}
              value={email}
              onChangeText={(text) => setEmail(text)}
              placeholder={'Email'}
              editable={false}
            />
            {/* <Image style={styles.editIcon} source={ic_edit} /> */}
          </View>

          {/* Address */}
          <View style={styles.viewBig}>
            <TextInput
              style={styles.inputBig}
              value={address}
              onChangeText={(text) => setAddress(text)}
              placeholder={'Address'}
              multiline={true}
              editable={false}
            />
          </View>
          {/* Button */}
          {/* <Button text="Submit" width={{ width: '80%' }} /> */}
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    fontSize: 26,
    color: heading_text,
    borderColor: heading_text,
    fontWeight: 'bold',
    paddingHorizontal: '10%',
  },
  viewSmall: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  inputSmall: {
    flexGrow: 1,
  },
  viewBig: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'flex-start',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  inputBig: {
    height: 130,
    flexGrow: 1,
    textAlignVertical: 'top',
  },
  editIcon: {
    width: 15,
    height: 15,
    marginRight: 15,
  },
  titleText: {
    fontSize: 26,
    paddingHorizontal: 10,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
  },
});
