import { useNavigation } from '@react-navigation/native';
import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {
  login_logo,
  login_image,
  ic_email,
  ic_password,
  ic_pshow,
  ic_phide,
} from '../constants/icons';
import { border_gray, heading_text, login_button, forgot_password } from '../constants/colors';
import { useState } from 'react';
import { Button } from '../components/Button';
import { ScrollView } from 'react-native-gesture-handler';
import { LOGIN } from '../constants/networkConstants';
import { Loader } from '../components/Loader';
import { ForgotPassword } from '../screens/ForgotPassword';
import { IS_LOGGED_IN, UserId } from '../constants/appConstants';

export const LoginScreen = (props) => {
  const [userId, setUserId] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [isPassHidden, setPassHidden] = useState(true);
  const { navigate } = useNavigation();

  const setLogin = async (loginData) => {
    try {
      const items = [
        [UserId, loginData],
        [IS_LOGGED_IN, 'true'],
      ];
      await AsyncStorage.multiSet(items, () => {
        //to do something
        navigate('DrawerNavigator', {
          screen: 'HomeScreen',
          params: { resetpassword: userId == password },
        });
        // navigate('DrawerNavigator', {
        //   resetpassword: userId == password,
        // });
        // alert(userId == password);
      });
    } catch (err) {
      console.log(err);
    }
  };
  const apiPostLogin = async () => {
    setLoading(true);
    var body = {
      UserId: userId,
      Password: password,
    };
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    console.log(JSON.stringify(body));
    await fetch(LOGIN, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setLoading(false);
        console.log(JSON.stringify(responseJson));
        if (responseJson.IsSuccess) {
          AsyncStorage.setItem('name', responseJson.Name);
          AsyncStorage.setItem('email', responseJson.Email);
          setLogin(JSON.stringify(responseJson.UserId));
        } else {
          alert(JSON.stringify(responseJson.Message));
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <ScrollView style={{ flex: 1 }}>
      {isLoading && <Loader />}
      <View style={{ flex: 1 }}>
        <View style={styles.topView}>
          <ImageBackground
            resizeMethod="auto"
            resizeMode="stretch"
            style={styles.bgImage}
            source={login_image}
          >
            <Image style={styles.bgLogo} source={login_logo} />
          </ImageBackground>
        </View>
        <View>{/* <Loader/> */}</View>
        <View style={styles.bottomView}>
          {/* Heading */}
          <View>
            <Text style={styles.loginHeading}>Login</Text>
          </View>

          {/* Input */}
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
              flex: 1,
            }}
          >
            {/* Email */}
            <View style={styles.containerBottom}>
              <ImageBackground style={styles.eIcon} source={ic_email} />
              <TextInput
                placeholder={'User Id'}
                style={styles.inputText}
                onChangeText={(userId) => setUserId(userId)}
              />
            </View>
            {/* Password */}
            <View style={styles.containerBottom}>
              <Image style={styles.pIcon} source={ic_password} />
              <TextInput
                placeholder={'Password'}
                style={styles.inputText}
                secureTextEntry={isPassHidden}
                onChangeText={(password) => setPassword(password)}
              />
              <TouchableOpacity
                onPress={() => {
                  setPassHidden(!isPassHidden);
                }}
              >
                <Image style={styles.psIcon} source={isPassHidden ? ic_pshow : ic_phide} />
              </TouchableOpacity>
            </View>
            {/* Button */}
            <Button onPress={apiPostLogin} text={'Login'} width={{ width: '80%' }} />
            {/* Forgot Password */}
            <View>
              <TouchableOpacity>
                <Text style={styles.forgotPassword} onPress={() => navigate('ForgotPassword')}>
                  FORGOT PASSWORD?
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  topView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: '1%',
    alignItems: 'center',
  },
  bgImage: {
    flex: 1,
    width: '100%',
    height: 280,
  },
  bgLogo: {
    flex: 1,
    height: '30%',
    width: '40%',
    marginTop: '2%',
    alignSelf: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
  },
  loginHeading: {
    fontSize: 26,
    color: heading_text,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  containerBottom: {
    justifyContent: 'center',
    marginBottom: 18,
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  inputText: {
    flexGrow: 1,
  },
  pIcon: {
    width: 12,
    height: 12,
    alignSelf: 'center',
    padding: 1,
    marginRight: 5,
  },
  eIcon: {
    width: 12,
    height: 10,
    alignSelf: 'center',
    padding: 1,
    marginRight: 5,
  },
  forgotPassword: {
    textAlign: 'center',
    fontSize: 12,
    color: forgot_password,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginTop: 20,
  },
  bottomView: {
    flex: 2,
    marginTop: 10,
    justifyContent: 'center',
  },
  psIcon: {
    width: 18,
    height: 12,
    marginRight: 5,
  },
});
