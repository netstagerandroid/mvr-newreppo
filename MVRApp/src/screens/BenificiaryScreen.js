import React from 'react';
import {
  Text,
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
  Image,
  Alert,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import {
  app_red,
  border_gray,
  heading_text,
  forgot_password,
  radio_button,
  upload_button,
} from '../constants/colors';
import { ic_cross } from '../constants/icons';
import { Button } from '../components/Button';
import { useRef } from 'react';
import { useState } from 'react';
import DocumentPicker from 'react-native-document-picker';
import { useEffect } from 'react';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import moment from 'moment';
export const BenificiaryScreen = (props) => {
  var date = new Date();
  var today = date.getDate() + '-' + parseInt(date.getMonth() + 1) + '-' + date.getFullYear();
  var lastday =
    parseInt(date.getDate() + 1) +
    '/' +
    parseInt(date.getMonth() + 1) +
    '/' +
    parseInt(date.getFullYear() - 60);
  const [dateOfBirth, setdateOfBirth] = useState();
  const [firstName, setFirstName] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [idNumber, setIdNumber] = useState();
  const [state1, setState1] = useState();
  const [units, setUnits] = useState();
  const [district, setDistrict] = useState();
  const [postOffice, setPostOffice] = useState();
  const [houseName, setHouseName] = useState();
  const [city, setCity] = useState();
  const [postCode, setPostCode] = useState();
  const [proofType, setProofType] = useState();
  const [selectFile1, setSelectFile] = useState();
  const [addressUpload, setAddressUpload] = useState(null);
  const [addressUpload1, setAddressproof] = useState(null);

  const [photoUpload, setPhotoUpload] = useState(null);
  const [birthProofUpload, setBirthProofUpload] = useState(null);
  const [birthProofUpload1, setBirthProof] = useState(null);

  const [plan, setPlan] = useState(props.planId);
  const [nominieeList, setNomineeList] = useState(props.data);
  const [nominieeRelation, setNominieeRelation] = useState(0);
  const [unitlist, setunitList] = useState([]);
  const uploadFile = async () => {
    if (singleFile != null) {
      const fileToUpload = singleFile;
      const data = new FormData();
      data.append('name', 'Image Upload');
      data.append('file_attachment', fileToUpload);

      let res = await fetch('', {
        method: 'post',
        body: data,
        headers: {
          'Content-Type': 'multipart/form-data;',
        },
      });
      let responseJson = await res.json();
      if (responseJson.status == 1) {
        console.log('Upload succesful');
      }
    } else {
      console.log('Please select file');
    }
  };

  const showcam = (type) => {
    Alert.alert('Select Image', 'Choose From ', [
      {
        text: 'Cancel',
        onPress: () => {
          alert('User cancelled image picker');
        },
        style: 'cancel',
      },
      {
        text: 'Camera',
        onPress: async () => {
          captureimage(type);
        },
      },
      {
        text: 'Gallery',
        onPress: async () => {
          selectFile(type);
        },
      },
    ]);
  };
  const captureimage = async (type) => {
    var options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },

      maxWidth: 500,
      maxHeight: 500,
      quality: 0.5,
    };

    await launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        // alert('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let res = response;
        switch (type) {
          case 'AddressUpload':
            setAddressUpload(res);
            break;
          case 'PhotoUpload':
            setPhotoUpload(res);
            break;
          case 'BirthProofUpload':
            setBirthProofUpload(res);
            break;

          case 'AddressUpload1':
            setAddressproof(res);
            break;

          case 'BirthProofUpload1':
            setBirthProof(res);
            break;
        }
        // alert('ImagePicker: ' + JSON.stringify(source));
      }
    });
  };
  const selectFile = async (type) => {
    var options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },

      maxWidth: 500,
      maxHeight: 500,
      quality: 0.5,
    };

    await launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
        // alert('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        alert('ImagePicker Error: ' + response.error);
      } else {
        let res = response;
        switch (type) {
          case 'AddressUpload':
            setAddressUpload(res);
            break;
          case 'PhotoUpload':
            setPhotoUpload(res);
            break;
          case 'BirthProofUpload':
            setBirthProofUpload(res);
            break;

          case 'AddressUpload1':
            setAddressproof(res);
            break;

          case 'BirthProofUpload1':
            setBirthProof(res);
            break;
        }
        // alert('ImagePicker: ' + JSON.stringify(source));
      }
    });
  };
  const selectFileOld = async (type) => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      switch (type) {
        case 'AddressUpload':
          setAddressUpload(res);
          break;
        case 'AddressUpload1':
          setAddressProof1(res);
        case 'PhotoUpload':
          setPhotoUpload(res);
          break;
        case 'BirthProofUpload':
          setBirthProofUpload(res);
          break;
        case 'BirthProofUpload1':
          setBirthProof1(res);
          break;
      }
      console.log('res :' + JSON.stringify(res));
      setSelectFile(res);
    } catch (err) {
      setSelectFile(null);
      if (DocumentPicker.isCancel(err)) {
        console.log('Canceled');
      } else {
        console.log('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  const isValidateIdNumber = () => {
    const reg = '^[0-9]{12,18}$';
    //(idNumber.length >= 12 ) && (idNumber.length <= 18)
    console.log(idNumber.length);
    if (idNumber.length >= 12 && idNumber.length <= 18) {
      return true;
    } else {
      return false;
    }
  };

  const validate = async () => {
    if (!firstName) {
      ToastAndroid.show('Enter a valid name', ToastAndroid.SHORT);
    } else if (!phoneNumber) {
      ToastAndroid.show('Enter a valid Phone Number', ToastAndroid.SHORT);
    } else if (phoneNumber.length != 10) {
      ToastAndroid.show('Enter a valid Phone Number', ToastAndroid.SHORT);
    } else if (!idNumber) {
      ToastAndroid.show('Enter ID Number', ToastAndroid.SHORT);
    } else if (!state1) {
      ToastAndroid.show('Select a State', ToastAndroid.SHORT);
    } else if (idNumber.length > 18) {
      ToastAndroid.show('Enter a valid ID Number', ToastAndroid.SHORT);
    } else if (!houseName) {
      ToastAndroid.show('Enter a valid House Name', ToastAndroid.SHORT);
    } else if (!postOffice) {
      ToastAndroid.show('Enter a valid Post office', ToastAndroid.SHORT);
    } else if (!city) {
      ToastAndroid.show('Enter a valid City', ToastAndroid.SHORT);
    } else if (!postCode) {
      ToastAndroid.show('Enter a valid Post Code', ToastAndroid.SHORT);
    } else if (postCode.length != 6) {
      ToastAndroid.show('Enter a valid Post Code', ToastAndroid.SHORT);
    }
    // else if (state == null) {
    //     ToastAndroid.show('Select state ', ToastAndroid.SHORT);
    // }
    else if (!district) {
      ToastAndroid.show('Please select a District', ToastAndroid.SHORT);
    } else if (plan?.Id == 2 && !nominieeRelation) {
      ToastAndroid.show('Select relationship', ToastAndroid.SHORT);
    } else if (!proofType) {
      ToastAndroid.show('Please select the type of Proof', ToastAndroid.SHORT);
    } else if (!addressUpload) {
      ToastAndroid.show('Please upload Address Proof', ToastAndroid.SHORT);
    } else if (!photoUpload) {
      ToastAndroid.show('Please upload Photo', ToastAndroid.SHORT);
    } else if (!birthProofUpload) {
      ToastAndroid.show('Please upload Birth Proof', ToastAndroid.SHORT);
    } else {
      var object = {
        tdFname: firstName,
        tdPhone: phoneNumber,
        tdAadhar: idNumber,
        tdUnits: units ? units : 0,
        tdHouse: houseName,
        tdStateId: state1.StateId,
        tdDistrictId: district.CityId,
        tdCity: city,

        tdPostCode: postCode,
        tddrRelation: nominieeRelation ? nominieeRelation.RelationshipId : 0,
        tddrProof: proofType.ProofId,
        PostOffice: postOffice,
        tdDOB: dateOfBirth,
        addressProof: {
          uri: addressUpload.uri,
          type: addressUpload.type,
          name: addressUpload.fileName,
        },
        addressProof3: addressUpload1
          ? {
              uri: addressUpload1?.uri,
              type: addressUpload1?.type,
              name: addressUpload1?.fileName,
            }
          : null,
        photo: {
          uri: photoUpload.uri,
          type: photoUpload.type,
          name: photoUpload.fileName,
        },
        birthProof: {
          uri: birthProofUpload.uri,
          type: birthProofUpload.type,
          name: birthProofUpload.fileName,
        },
        birthProof3: birthProofUpload1
          ? {
              uri: birthProofUpload1?.uri,
              type: birthProofUpload1?.type,
              name: birthProofUpload1?.fileName,
            }
          : null,
      };
      console.log(object.tdUnits);
      await setNomineeList(nominieeList.concat(object));

      console.log(object.tdFname);
      // var totalunit = [];
      //         totalunit = nominieeList;
      //         var grandunits = [];
      //         totalunit.forEach((element) => {
      //           grandunits.push(element.tdUnits);
      //         });
      //         setunitList(grandunits);

      ToastAndroid.show('Beneficiary Added', ToastAndroid.SHORT);
      return true;
    }
  };
  const clearAll = async () => {
    setFirstName();
    setPhoneNumber();
    setIdNumber();
    setUnits();
    setState1('');
    setHouseName();
    setDistrict(0);
    setCity();
    setPostCode();
    setNominieeRelation(0);
    setProofType(0);
    setPostOffice();
    setdateOfBirth();
    setAddressUpload();
    setPhotoUpload();
    setBirthProofUpload();
    setAddressproof();
    setBirthProof();
  };
  const removeNominiee = (index) => {
    Alert.alert('Remove', 'Selected benificiary will be removed', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: async () => {
          var temp = nominieeList;
          temp.splice(index, 1);
          await setNomineeList([]);
          await setNomineeList(temp);
        },
      },
    ]);
  };

  useEffect(() => {}, [nominieeList]);
  useEffect(() => {}, [plan]);

  return (
    <View>
      <ScrollView>
        <View style={{ marginTop: 40, width: '80%', flexDirection: 'row' }}>
          <Text style={styles.container}>Benificiary</Text>
          <TouchableOpacity
            onPress={async () => await props.close(nominieeList)}
            style={{ marginRight: 10, alignSelf: 'center' }}
          >
            <Image style={{ width: 25, height: 25 }} source={ic_cross}></Image>
          </TouchableOpacity>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              placeholder="Name *"
              value={firstName}
              onChangeText={(text) => setFirstName(text)}
            />
          </View>

          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              placeholder="Phone Number *"
              keyboardType={'numeric'}
              value={phoneNumber}
              onChangeText={(text) => setPhoneNumber(text)}
            />
          </View>
          <View style={{ width: '80%' }}>
            <View style={{ alignItems: 'flex-start', marginBottom: 10 }}>
              {/* <Text>hi{lastday.toString()}</Text> */}
              <DatePicker
                mode="date"
                placeholder="DD-MM-YYYY"
                date={dateOfBirth}
                format="DD/MM/YYYY"
                minDate={lastday}
                maxDate={today}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    height: 0,
                    width: 0,
                  },
                  dateInput: {
                    marginBottom: 10,
                    height: 45,
                    width: 150,
                    borderRadius: 10,
                    borderWidth: 1,
                    color: heading_text,
                    borderColor: border_gray,
                    marginTop: 10,
                  },
                }}
                onDateChange={(v) => {
                  var then = lastday;
                  var now = v;
                  var ms = moment(now, 'DD/MM/YYYY HH:mm:ss').isBefore(
                    moment(then, 'DD/MM/YYYY HH:mm:ss')
                  );
                  if (ms) {
                    alert(`This date of birth is not supported. Select after ${lastday}`);
                  } else {
                    setdateOfBirth(v);
                  }
                  console.log('' + ms);
                }}
              />
            </View>
          </View>

          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              placeholder="ID Proof Number *"
              value={idNumber}
              // keyboardType={'numeric'}
              onChangeText={(text) => {
                setIdNumber(text);
                console.log(text.length);
              }}
            />
          </View>

          {(plan?.Id == 4 || plan?.Id != 2) && (
            <View style={styles.textBox}>
              <TextInput
                style={styles.input}
                placeholder="Units "
                value={units}
                keyboardType={'numeric'}
                onChangeText={(text) => {
                  setUnits(text);
                  console.log(text);
                }}
              />
            </View>
          )}
          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              placeholder="Post Office *"
              value={postOffice}
              onChangeText={(text) => setPostOffice(text)}
            />
          </View>

          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              placeholder="House Name *"
              value={houseName}
              onChangeText={(text) => setHouseName(text)}
            />
          </View>

          <View style={styles.pickerView}>
            <RNPickerSelect
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 20,
                  right: 10,
                  color: 'red',
                },
                placeholder: {
                  color: 'black',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              placeholder={{ label: 'Select State', value: null }}
              value={state1}
              // disabled={true}
              onValueChange={(value) => setState1(value)}
              items={props.states}
            />
          </View>

          <View style={styles.pickerView}>
            <RNPickerSelect
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 20,
                  right: 10,
                  color: 'red',
                },
                placeholder: {
                  color: 'black',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              placeholder={{ label: 'Select District', value: null }}
              value={district}
              onValueChange={(value) => setDistrict(value)}
              items={props.districts}
            />
          </View>

          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              placeholder="City *"
              value={city}
              onChangeText={(text) => setCity(text)}
            />
          </View>

          <View style={styles.textBox}>
            <TextInput
              style={styles.input}
              keyboardType={'numeric'}
              placeholder="Post Code *"
              value={postCode}
              onChangeText={(text) => setPostCode(text)}
            />
          </View>
          {(plan?.Id != 4 || plan?.Id == 2) && (
            <View style={styles.pickerView}>
              <RNPickerSelect
                style={{
                  ...pickerSelectStyles,
                  iconContainer: {
                    top: 20,
                    right: 10,
                    color: 'red',
                  },
                  placeholder: {
                    color: 'black',
                    fontSize: 12,
                    fontWeight: 'bold',
                  },
                }}
                useNativeAndroidPickerStyle={true}
                placeholder={{ label: 'Select Relationship', value: null }}
                value={nominieeRelation}
                onValueChange={(value) => {
                  setNominieeRelation(value);
                  // alert(JSON.stringify(value));
                }}
                items={props.relationList}
              />
            </View>
          )}

          <Text style={styles.titleText}>
            Type of Proof <Text style={{ color: app_red }}>*</Text>
          </Text>

          <View style={styles.pickerView}>
            <RNPickerSelect
              style={{
                ...pickerSelectStyles,
                iconContainer: {
                  top: 20,
                  right: 10,
                  color: 'red',
                },
                placeholder: {
                  color: 'black',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              placeholder={{ label: 'Select Address Proof', value: null }}
              value={proofType}
              onValueChange={(value) => setProofType(value)}
              items={props.proofList}
            />
          </View>

          <Text style={styles.titleText}>
            Upload Address Proof <Text style={{ color: app_red }}>*</Text>
          </Text>

          <View style={styles.textBox}>
            <TouchableOpacity onPress={() => showcam('AddressUpload')} style={styles.touchable}>
              <Text style={{ marginTop: 8, marginLeft: 18 }}>Upload Files</Text>
              <Text style={{ marginTop: 8, textAlign: 'right', marginLeft: 50, fontSize: 12 }}>
                {addressUpload ? 'selected' : 'Choose a File'}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.textBox}>
            <TouchableOpacity onPress={() => showcam('AddressUpload1')} style={styles.touchable}>
              <Text style={{ marginTop: 8, marginLeft: 18 }}>Upload Files</Text>
              <Text style={{ marginTop: 8, textAlign: 'right', marginLeft: 50, fontSize: 12 }}>
                {addressUpload1 ? 'selected' : 'Choose a File'}
              </Text>
            </TouchableOpacity>
          </View>

          <Text style={styles.titleText}>
            Upload Photo <Text style={{ color: app_red }}>*</Text>
          </Text>

          <View style={styles.textBox}>
            <TouchableOpacity onPress={() => showcam('PhotoUpload')} style={styles.touchable}>
              <Text style={{ marginTop: 8, marginLeft: 18 }}>Upload Files</Text>
              <Text style={{ marginTop: 8, textAlign: 'right', marginLeft: 50, fontSize: 12 }}>
                {photoUpload ? 'selected' : 'Choose a File'}
              </Text>
            </TouchableOpacity>
          </View>

          <Text style={styles.titleText}>
            Birth Proof <Text style={{ color: app_red }}>*</Text>
          </Text>

          <View style={styles.textBox}>
            <TouchableOpacity onPress={() => showcam('BirthProofUpload')} style={styles.touchable}>
              <Text style={{ marginTop: 8, marginLeft: 18 }}>Upload Files</Text>
              <Text style={{ marginTop: 8, textAlign: 'right', marginLeft: 50, fontSize: 12 }}>
                {birthProofUpload ? 'selected' : 'Choose a File'}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.textBox}>
            <TouchableOpacity onPress={() => showcam('BirthProofUpload1')} style={styles.touchable}>
              <Text style={{ marginTop: 8, marginLeft: 18 }}>Upload Files</Text>
              <Text style={{ marginTop: 8, textAlign: 'right', marginLeft: 50, fontSize: 12 }}>
                {birthProofUpload1 ? 'selected' : 'Choose a File'}
              </Text>
            </TouchableOpacity>
          </View>

          <Button
            text="Add Benificiary"
            width={{ width: '80%' }}
            onPress={() => validate().then((v) => v === true && clearAll())}
          />
          <TouchableOpacity
            onPress={async () => {
              await props.close(nominieeList);
              console.log(JSON.stringify(nominieeList));
            }}
          >
            <Text style={styles.clearAll}>Done</Text>
          </TouchableOpacity>
        </View>
        <View style={{ width: '100%', marginTop: 30 }}>
          <FlatList
            data={nominieeList}
            renderItem={({ item, index }) => (
              <View
                key={index}
                style={{
                  width: '80%',
                  alignSelf: 'center',
                  paddingVertical: 8,
                  flexDirection: 'row',
                  flex: 1,
                  borderRadius: 5,
                  marginBottom: 8,
                  borderColor: 'black',
                  borderWidth: 1,
                  marginHorizontal: 10,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <Text style={{ marginHorizontal: 10, fontSize: 18 }}>{item.tdFname}</Text>
                <TouchableOpacity
                  onPress={() => removeNominiee(index)}
                  style={{ marginRight: 10, alignSelf: 'center' }}
                >
                  <Image style={{ width: 25, height: 25 }} source={ic_cross}></Image>
                </TouchableOpacity>
              </View>
            )}
            keyExtractor={(item) => item.firstName}
          />
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    fontSize: 26,
    color: heading_text,
    borderColor: heading_text,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  titleText: {
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
    marginTop: 10,
    fontWeight: 'bold',
    marginBottom: 10,
    marginLeft: 35,
  },
  textBox: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  upLoadTouchable: {
    backgroundColor: upload_button,
    borderWidth: 0,
    borderColor: border_gray,
    paddingVertical: 8,
    paddingHorizontal: 13,
    alignSelf: 'center',
    borderRadius: 10,
  },
  fileName: {
    textAlign: 'right',
    width: '60%',
  },
  input: {
    flexGrow: 1,
  },
  // textBoxDOB3: {
  //   marginBottom: 10,
  //   width: '36%',
  //   borderRadius: 10,
  //   borderWidth: 1,
  //   flexDirection: 'row',
  //   color: heading_text,
  //   borderColor: border_gray,
  //   paddingHorizontal: 10,
  // },
  clearAll: {
    textAlign: 'center',
    fontSize: 16,
    color: forgot_password,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginTop: 20,
    marginBottom: 20,
  },
  picker: {
    justifyContent: 'center',
    marginBottom: 10,
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  pickerView: {
    width: '80%',
    color: heading_text,
    borderColor: border_gray,
    borderRadius: 10,
    borderWidth: 1,
    marginBottom: 10,
  },
  textBoxDOB3: {
    marginBottom: 10,
    width: '36%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  touchable: {
    backgroundColor: upload_button,
    borderWidth: 0,
    borderColor: border_gray,
    height: 40,
    width: 130,
    borderRadius: 10,
    marginTop: 10,
    marginRight: 130,
    marginBottom: 8,
    flexDirection: 'row',
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'black',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
