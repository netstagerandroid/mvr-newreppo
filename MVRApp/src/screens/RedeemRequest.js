import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Share,
} from 'react-native';

import { ic_calender } from '../constants/icons';
import { app_red, border_gray, item_background_gray } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';
import { AppBar } from '../components/AppBar';
import { Button } from '../components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DatePicker from 'react-native-datepicker';
import { useRef } from 'react';

export const RedeemRequest = (props) => {
  const { navigate } = useNavigation();
  var datePicker = useRef();
  var date = new Date();
  var today = date.getDate() + '-' + parseInt(date.getMonth() + 1) + '-' + date.getFullYear();
  const [expectedDate, setExpectedDate] = useState(today);
  const [requestAmount, setRequestAmount] = useState('');

  const initScrn = async () => await AsyncStorage.setItem('screen', 'GenerateLinkScreen');
  initScrn();

  return (
    <View style={styles.container}>
      <AppBar />

      <DatePicker
        style={{ width: 0, height: 0 }}
        ref={(ref) => {
          datePicker = ref;
        }}
        date={today}
        mode="date"
        placeholder="select date"
        format="DD-MM-YYYY"
        minDate={today}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0,
          },
          dateInput: {
            marginLeft: 36,
          },
        }}
        onDateChange={(date) => {
          setExpectedDate(date);
        }}
      />

      <ScrollView showsHorizontalScrollIndicator={false}>
        <Text style={styles.titleText}>Redeem Request</Text>
        <Text style={styles.inputTitleText}>
          Request Amout <Text style={{ color: app_red }}>*</Text>
        </Text>
        <View style={styles.searchBar}>
          <TextInput
            style={styles.textInput}
            returnKeyType="next"
            keyboardType="number-pad"
            onSubmitEditing={() => alert('')}
            placeholder={'₹ 50,000'}
            value={requestAmount && '₹' + requestAmount}
            onChangeText={(text) => {
              setRequestAmount(text.trim().replace('₹', ''));
            }}
          />
          {/* <Image resizeMode="contain" style={styles.searchIcon} source={ic_calender} /> */}
        </View>
        <Text style={styles.inputTitleText}>
          Expected Date <Text style={{ color: app_red }}>*</Text>
        </Text>
        <View style={styles.searchBar}>
          <TextInput
            style={styles.textInput}
            returnKeyType="next"
            placeholder={today}
            value={expectedDate}
            onFocus={() => datePicker.onPressDate()}
            onChangeText={(text) => {}}
          />
          {/* <Image resizeMode="contain" style={styles.searchIcon} source={ic_calender} /> */}
        </View>
        <View style={styles.buttonsView}>
          <Button onPress={() => {}} width={{ width: '80%' }} text={'Submit'} />
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    // backgroundColor: app_background,
    flex: 1,
  },
  searchBar: {
    flexDirection: 'row',
    width: '80%',
    height: 50,
    borderColor: border_gray,
    borderWidth: 1,
    borderRadius: 15,
    alignSelf: 'center',
    paddingHorizontal: 10,
  },
  titleText: {
    fontSize: 26,
    paddingHorizontal: 10,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
  },
  textInput: {
    paddingHorizontal: 15,
    flexGrow: 1,
  },
  buttonsView: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  shareButton: {
    backgroundColor: app_red,
    height: 45,
    width: 50,
    marginStart: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
  },
  redeemBorder: {
    borderWidth: 0.5,
    borderRadius: 9,
    width: '100%',
    height: '100%',
    borderColor: '#FBF7F7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    padding: 10,
  },
  inputTitleText: {
    fontSize: 13,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '78%',
    marginTop: 20,
  },
  searchIcon: {
    width: 20,
    height: 20,
    padding: 5,
    alignSelf: 'center',
  },
});
