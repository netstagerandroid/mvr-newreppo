import React from 'react';
import { View, Text, TouchableOpacity, TextInput, StyleSheet, ToastAndroid } from 'react-native';
import { AppBar } from '../components/AppBar';
import { useNavigation } from '@react-navigation/native';
import { Button } from '../components/Button';
import { useState } from 'react';
import { border_gray, heading_text, forgot_password } from '../constants/colors';
import { UserId } from '../constants/appConstants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { FORGOT_PASSWORD } from '../constants/networkConstants';
import { Loader } from '../components/Loader';

export const ForgotPassword = (props) => {
  const { navigate } = useNavigation();
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState();
  const [isLoading, setLoading] = useState(false);
  const apiCallForgotPassword = async () => {
    setLoading(true);
    var uId = '';
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    var body = {
      Email: email,
    };

    await fetch(FORGOT_PASSWORD, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(body),
    })
      .then((responsea) => {
        console.log(responsea);
        return Promise.all([responsea.status, responsea.json()]);
      })
      .then((responseJson) => {
        console.log(responseJson);
        setLoading(false);
        if (responseJson[0] === 200)
          if (responseJson[1].IsSuccess) {
            setMessage(responseJson[1].Message);
            // ToastAndroid.show(responseJson[1].Message, ToastAndroid.SHORT);
          } else {
            setMessage(responseJson[1].Message);
          }
        else {
          alert(responseJson[1].Message);
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <View style={{ flex: 1 }}>
      <AppBar hideButtons={true} />
      {/* Heading */}
      {isLoading && <Loader />}
      <View style={{ marginTop: 40 }}>
        <Text style={styles.container}>Forgot Password</Text>
      </View>

      <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
        {/* Text Input */}
        <View style={{ marginBottom: 20 }}>
          <View style={styles.viewSmall}>
            <TextInput
              placeholder={'Email'}
              style={styles.inputSmall}
              onChangeText={(text) => setEmail(text)}
            />
          </View>
          {message && <Text style={styles.emailNotFound}>{message}</Text>}
        </View>

        {/* Button */}
        <Button
          text={'Submit'}
          width={{ width: '80%' }}
          onPress={() => {
            if (email.length > 0) apiCallForgotPassword();
            else alert('Enter registered email');
            // ToastAndroid.show('Enter registerd email', ToastAndroid.SHORT);
          }}
        />
        {/* back */}
        <View>
          <TouchableOpacity>
            <Text style={styles.backHome} onPress={() => navigate('LoginScreen')}>
              {' '}
              {'<'} BACK TO LOGIN{' '}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    fontSize: 26,
    color: heading_text,
    borderColor: heading_text,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  viewSmall: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  inputSmall: {
    flexGrow: 1,
  },
  backHome: {
    textAlign: 'center',
    fontSize: 13,
    color: forgot_password,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginTop: 20,
  },
  emailNotFound: {
    fontSize: 12,
    color: forgot_password,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginTop: 20,
  },
});
