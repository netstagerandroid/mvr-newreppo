import React from 'react';
import { View, Text, StyleSheet, TextInput, Image, ToastAndroid, BackHandler } from 'react-native';
import { Button } from '../components/Button';
import { AppBar } from '../components/AppBar';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { ic_phide, ic_pshow } from '../constants/icons';
import { border_gray, heading_text } from '../constants/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useState } from 'react';
import { CHANGE_PASSWORD } from '../constants/networkConstants';
import { UserId } from '../constants/appConstants';
import { useNavigation } from '@react-navigation/native';
import { Loader } from '../components/Loader';
import { useEffect } from 'react';

export const ChangePassword = (props) => {
  const { navigate } = useNavigation();
  const initScrn = async () => await AsyncStorage.setItem('screen', 'ChangePassword');

  initScrn();

  const [isLoading, setLoading] = useState(false);
  const [userId, setUserId] = useState(false);
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isNewPassHidden, setNewPassHidden] = useState(true);
  const [isConfirmPassHidden, setConfirmPassHidden] = useState(true);
  const [isOldPassHidden, setOldPassHidden] = useState(true);

  const validate = () => {
    if (!oldPassword.length > 0) {
      ToastAndroid.show('Enter Old password', ToastAndroid.SHORT);
    } else if (!newPassword.length > 0) {
      ToastAndroid.show('Enter New password', ToastAndroid.SHORT);
    } else if (newPassword.length < 5) {
      ToastAndroid.show('Minimum 6 charecter required', ToastAndroid.SHORT);
    } else if (!confirmPassword.length > 0) {
      ToastAndroid.show('Enter password again', ToastAndroid.SHORT);
    } else if (confirmPassword !== newPassword) {
      ToastAndroid.show("Password don't match", ToastAndroid.SHORT);
    } else {
      apiChangePassword();
    }
  };

  const apiChangePassword = async () => {
    setLoading(true);
    var uId = '';
    var headers = new Headers();
    headers.append('content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    async () =>
      await AsyncStorage.getItem(UserId).then((id) => {
        setUserId(id);
        uId = id;
        console.log('++++++',uId)
      });
    var body = {
      // UserId: userId,
      // OldPassword: oldPassword,
      // NewPassword: newPassword,
      // ConfirmPassword: confirmPassword,
      UserId: 0,
  FirstName: "string",
  LastName: "string",
  Mobile: "string",
  Email: "string",
  Address: "string",
  OldPassword: oldPassword,
  NewPassword: newPassword,
  ConfirmPassword: confirmPassword,

    };
    

    await fetch(CHANGE_PASSWORD, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(body),
    })

    // alert(body)
    // console.log(body)
    
      .then((responsea) => {
        console.log(responsea);
        return Promise.all([responsea.status, responsea.json()]);
      })
      .then((responseJson) => {
        console.log(responseJson);
        setLoading(false);
        if (responseJson[0] === 200)
          if (responseJson[1].IsSuccess) {
            alert('Password Changed successfully')
            navigate('LoginScreen');
          } else {
            alert(responseJson[1].Message);
            // alert(JSON.stringify(responseJson))
            // alert('hello')
          }
        else if (responseJson[0] === 401) {
          AsyncStorage.clear().then(() => navigate('LoginScreen'));
        } else {
           alert(responseJson[1].Message);
          //  alert(JSON.stringify(responseJson))
          // alert('hi')

        }
      })

      
      .catch((err) => console.log(err));
  };

  return (
    <View style={{ flex: 1 }}>
      <AppBar />
      {/* Heading */}
      {isLoading && <Loader />}
      <ScrollView style={{ flex: 1 }}>
        <View style={{ marginTop: 40 }}>
          <Text style={styles.container}>Change Password</Text>
        </View>
        {/* Input fields */}
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 40,
          }}
        >
          {/* Old Password */}
          <View style={styles.viewPassword}>
            <TextInput
              secureTextEntry={isOldPassHidden}
              value={oldPassword}
              onChangeText={(text) => {
                setOldPassword(text);
              }}
              style={styles.inputPassword}
              placeholder={'Old Password'}
            />
            <TouchableOpacity
              onPress={() => {
                setOldPassHidden(!isOldPassHidden);
              }}
            >
              <Image style={styles.psIcon} source={isOldPassHidden ? ic_pshow : ic_phide} />
            </TouchableOpacity>
          </View>
          {/* New Password */}
          <View style={styles.viewPassword}>
            <TextInput
              secureTextEntry={isNewPassHidden}
              style={styles.inputPassword}
              value={newPassword}
              placeholder={'New Password'}
              onChangeText={(text) => {
                setNewPassword(text);
              }}
            />
            <TouchableOpacity
              onPress={() => {
                setNewPassHidden(!isNewPassHidden);
              }}
            >
              <Image style={styles.psIcon} source={isNewPassHidden ? ic_pshow : ic_phide} />
            </TouchableOpacity>
          </View>
          {/* Confirm Password */}
          <View style={styles.viewPassword}>
            <TextInput
              secureTextEntry={isConfirmPassHidden}
              style={styles.inputPassword}
              value={confirmPassword}
              placeholder={'Confirm Password'}
              onChangeText={(text) => {
                setConfirmPassword(text);
              }}
            />
            <TouchableOpacity
              onPress={() => {
                setConfirmPassHidden(!isConfirmPassHidden);
              }}
            >
              <Image style={styles.psIcon} source={isConfirmPassHidden ? ic_pshow : ic_phide} />
            </TouchableOpacity>
          </View>
          {/* Button */}
          <Button text="Update" width={{ width: '80%' }} onPress={validate} />
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    fontSize: 26,
    color: heading_text,
    borderColor: heading_text,
    fontWeight: 'bold',
    paddingHorizontal: '10%',
  },
  viewPassword: {
    justifyContent: 'center',
    marginBottom: 18,
    alignItems: 'center',
    width: '80%',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    color: heading_text,
    borderColor: border_gray,
    paddingHorizontal: 10,
  },
  inputPassword: {
    flexGrow: 1,
  },
  psIcon: {
    width: 17,
    height: 12,
    marginRight: 15,
  },
  phIcon: {
    width: 17,
    height: 16,
    marginRight: 15,
  },
});
