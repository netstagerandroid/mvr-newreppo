import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Share,
  ToastAndroid,
  BackHandler,
  Alert,
} from 'react-native';

import { ic_calender } from '../constants/icons';
import { app_red, border_gray, item_background_gray } from '../constants/colors';
import { useNavigation } from '@react-navigation/native';

import { AppBar } from '../components/AppBar';
import { Button } from '../components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';

import WebView from 'react-native-webview';
import { BASE_URL } from '../constants/networkConstants';

export const PaymentGatewayScreen = (props) => {
  const { navigate } = useNavigation();
  var date = new Date();
  var today = date.getDate() + '-' + parseInt(date.getMonth() + 1) + '-' + date.getFullYear();

  const [requestAmount, setRequestAmount] = useState('');

  const initScrn = async () => await AsyncStorage.setItem('screen', 'GenerateLinkScreen');
  initScrn();

  // var uri = new URL('https://masscare.calicutcitybank.com/Failure.aspx?id=E005').pathname;
  // console.log(JSON.stringify(uri));
  // console.log(uri);
  function handleBackButtonClick() {
    navigate('HomeScreen');
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
  console.log(JSON.stringify(props.route.params.response), '#########');
  // alert(JSON.stringify(props.route.params.response.encryptredirecturl))
  const exitpage = (v) => {
    Alert.alert('Exit', 'Exitfrom This page', [
      {
        text: 'ok',
        onPress: async () => {
          //  uri.pathname
          props.navigation.navigate('HomeScreen');
          {
          }
        },
      },
    ]);
  };
  return (
    <View style={styles.container}>
      <AppBar hideButtons={true} />
      <WebView
        style={{ flex: 1, width: '100%', height: '100%' }}
        //   ref="webview"
        source={{
          //  uri: props.route.params.response?.encryptredirecturl,
          // uri: props.route.params.response.redirecturl,
          uri: props.route.params.response?.encryptredirecturl,
        }}
        onNavigationStateChange={(v) => {
          if (v.url == 'https://masscare.calicutcitybank.com/Success.aspx') {
            setTimeout(() => {
            
              // exitpage(v);
            }, 4000);
            ToastAndroid.show('Please wait ...', ToastAndroid.SHORT);
          } else if (v.url == 'https://masscare.calicutcitybank.com/Failure.aspx') {
            setTimeout(() => {
              // props.navigation.navigate('HomeScreen');
         
              exitpage(v);
            }, 4000);
            ToastAndroid.show('Please wait ...', ToastAndroid.SHORT);
          }
          if (v.url == 'https://masscare.calicutcitybank.com/Failure.aspx?id=E005') {
            setTimeout(() => {
              // props.navigation.navigate('HomeScreen');
              // exitpage(v);
             

            }, 4000);
            ToastAndroid.show('Please wait ...', ToastAndroid.SHORT);
          }
        
          // console.log(v.url);
          // console.log(v);

          // For success:  http://mvr.netstager.com/Success.aspx
          // For failure: http://mvr.netstager.com/Failure.aspx
          // var uri = new URL('https://masscare.calicutcitybank.com/Failure.aspx?id=E005');
        }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={false}
        scrollEnabled
        scalesPageToFit
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchBar: {
    flexDirection: 'row',
    width: '80%',
    height: 50,
    borderColor: border_gray,
    borderWidth: 1,
    borderRadius: 15,
    alignSelf: 'center',
    paddingHorizontal: 10,
  },
  titleText: {
    fontSize: 26,
    paddingHorizontal: 10,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '90%',
  },
  textInput: {
    paddingHorizontal: 15,
    flexGrow: 1,
  },
  buttonsView: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  shareButton: {
    height: 45,
    width: 50,
    marginStart: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
  },
  redeemBorder: {
    borderWidth: 0.5,
    borderRadius: 9,
    width: '100%',
    height: '100%',
    borderColor: '#FBF7F7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    padding: 10,
  },
  inputTitleText: {
    fontSize: 13,
    marginVertical: 5,
    alignSelf: 'center',
    textAlign: 'left',
    width: '78%',
    marginTop: 20,
  },
  searchIcon: {
    width: 20,
    height: 20,
    padding: 5,
    alignSelf: 'center',
  },
});
